#ifdef USE_TMP75
// derived from https://github.com/parlotto/TMP75 by SCL
#ifndef _TMP75_H
#define _TMP75_H

#ifndef TEMPERATURE_NOT_INSTALLED
#define TEMPERATURE_NOT_INSTALLED -2560
#endif


#define TMP75_DEFAULT_CONFIG 0x60 // 12 bits , 0 fault queue, ALERT low polarity, comparator mode  

// registers pointers
#define TMP75_TEMP_REG 0
#define TMP75_CONF_REG 1
#define TMP75_TLOW_REG 2
#define TMP75_THIGH_REG 3


class TMP75 {
  bool _isPresent;
 public :
  TMP75();
  int begin();
  int16_t read(int16_t *unscaled=NULL); // return C*10, unscaled =  C*16.
  //  void setHighLimit( float highLimit );
  //  void setLowLimit(  float lowLimit );
  bool isPresent() { return _isPresent; }
};

#endif // _TMP75_H_
#endif // USE_TMP75
