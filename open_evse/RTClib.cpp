// Code by JeeLabs http://news.jeelabs.org/code/
// Released to the public domain! Enjoy!

#include "./Wire.h"

#define SECONDS_PER_DAY 86400L

#define SECONDS_FROM_1970_TO_2000 946684800

#if (ARDUINO >= 100)
 #include <Arduino.h> // capital A so it is error prone on case-sensitive filesystems
#else
 #include <WProgram.h>
#endif

#include "open_evse.h"
#include "RTCLib.h"

int i0 = 0; //The new wire library needs to take an int when you are sending for the zero register
////////////////////////////////////////////////////////////////////////////////
// utility code, some of this could be exposed in the DateTime API if needed

const uint8_t daysInMonth [] PROGMEM = { 31,28,31,30,31,30,31,31,30,31,30,31 }; //has to be const or compiler compaints

// number of days since 2000/01/01, valid for 2001..2099
static uint16_t date2days(uint16_t y, uint8_t m, uint8_t d) {
    if (y >= 2000)
        y -= 2000;
    uint16_t days = d;
    for (uint8_t i = 1; i < m; ++i)
        days += pgm_read_byte(daysInMonth + i - 1);
    if (m > 2 && y % 4 == 0)
        ++days;
    return days + 365 * y + (y + 3) / 4 - 1;
}

static long time2long(uint16_t days, uint8_t h, uint8_t m, uint8_t s) {
    return ((days * 24L + h) * 60 + m) * 60 + s;
}

////////////////////////////////////////////////////////////////////////////////
// DateTime implementation - ignores time zones and DST changes
// NOTE: also ignores leap seconds, see http://en.wikipedia.org/wiki/Leap_second

DateTime::DateTime (uint32_t t) {
  t -= SECONDS_FROM_1970_TO_2000;    // bring to 2000 timestamp from 1970

    ss = t % 60;
    t /= 60;
    mm = t % 60;
    t /= 60;
    hh = t % 24;
    uint16_t days = t / 24;
    uint8_t leap;
    for (yOff = 0; ; ++yOff) {
        leap = yOff % 4 == 0;
        if (days < 365u + leap)
            break;
        days -= 365 + leap;
    }
    for (m = 1; ; ++m) {
        uint8_t daysPerMonth = pgm_read_byte(daysInMonth + m - 1);
        if (leap && m == 2)
            ++daysPerMonth;
        if (days < daysPerMonth)
            break;
        days -= daysPerMonth;
    }
    d = days + 1;
}

DateTime::DateTime (uint16_t year, uint8_t month, uint8_t day, uint8_t hour, uint8_t min, uint8_t sec) {
    if (year >= 2000)
        year -= 2000;
    yOff = year;
    m = month;
    d = day;
    hh = hour;
    mm = min;
    ss = sec;
}

static uint8_t conv2d(const char* p) {
    uint8_t v = 0;
    if ('0' <= *p && *p <= '9')
        v = *p - '0';
    return 10 * v + *++p - '0';
}

// A convenient constructor for using "the compiler's time":
//   DateTime now (__DATE__, __TIME__);
// NOTE: using PSTR would further reduce the RAM footprint
DateTime::DateTime (const char* date, const char* time) {
    // sample input: date = "Dec 26 2009", time = "12:34:56"
    yOff = conv2d(date + 9);
    // Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec 
    switch (date[0]) {
    case 'J': m = (date[1] == 'a') ? 1 : ((date[2] == 'n') ? 6 : 7); break;
        case 'F': m = 2; break;
        case 'A': m = date[2] == 'r' ? 4 : 8; break;
        case 'M': m = date[2] == 'r' ? 3 : 5; break;
        case 'S': m = 9; break;
        case 'O': m = 10; break;
        case 'N': m = 11; break;
        case 'D': m = 12; break;
    }
    d = conv2d(date + 4);
    hh = conv2d(time);
    mm = conv2d(time + 3);
    ss = conv2d(time + 6);
}

uint8_t DateTime::dayOfWeek() const {    
    uint16_t day = date2days(yOff, m, d);
    return (day + 6) % 7; // Jan 1, 2000 is a Saturday, i.e. returns 6
}

uint32_t DateTime::unixtime(void) const {
  uint32_t t;
  uint16_t days = date2days(yOff, m, d);
  t = time2long(days, hh, mm, ss);
  t += SECONDS_FROM_1970_TO_2000;  // seconds from 1970 to 2000

  return t;
}

static uint8_t bcd2bin (uint8_t val) { return val - 6 * (val >> 4); }
static uint8_t bin2bcd (uint8_t val) { return val + 6 * (val / 10); }

#ifdef USE_RV3028
//REGISTERS
//Clock registers
#define RV3028_SECONDS      			0x00
#define RV3028_MINUTES      			0x01
#define RV3028_HOURS        			0x02
//Calendar registers
#define RV3028_WEEKDAY				0x03
#define RV3028_DATE         			0x04
#define RV3028_MONTHS        			0x05
#define RV3028_YEARS        			0x06

//Alarm registers
#define RV3028_MINUTES_ALM     			0x07
#define RV3028_HOURS_ALM       			0x08
#define RV3028_DATE_ALM        			0x09

//Periodic Countdown Timer registers
#define RV3028_TIMERVAL_0				0x0A
#define RV3028_TIMERVAL_1				0x0B
#define RV3028_TIMERSTAT_0				0x0C
#define RV3028_TIMERSTAT_1				0x0D

//Configuration registers
#define RV3028_STATUS					0x0E
#define RV3028_CTRL1					0x0F
#define RV3028_CTRL2					0x10
#define RV3028_GPBITS					0x11
#define RV3028_INT_MASK					0x12

//Eventcontrol/Timestamp registers
#define RV3028_EVENTCTRL				0x13
#define RV3028_COUNT_TS					0x14
#define RV3028_SECONDS_TS				0x15
#define RV3028_MINUTES_TS				0x16
#define RV3028_HOURS_TS					0x17
#define RV3028_DATE_TS					0x18
#define RV3028_MONTH_TS					0x19
#define RV3028_YEAR_TS					0x1A

//Unix Time registers
#define RV3028_UNIX_TIME0				0x1B
#define RV3028_UNIX_TIME1				0x1C
#define RV3028_UNIX_TIME2				0x1D
#define RV3028_UNIX_TIME3				0x1E

//RAM registers
#define RV3028_USER_RAM1				0x1F
#define RV3028_USER_RAM2				0x20

//Password registers
#define RV3028_PASSWORD0				0x21
#define RV3028_PASSWORD1				0x22
#define RV3028_PASSWORD2				0x23
#define RV3028_PASSWORD3				0x24

//EEPROM Memory Control registers
#define RV3028_EEPROM_ADDR				0x25
#define RV3028_EEPROM_DATA				0x26
#define RV3028_EEPROM_CMD				0x27

//ID register
#define RV3028_ID						0x28

//EEPROM Registers
#define EEPROM_Clkout_Register			0x35
#define RV3028_EEOffset_8_1				0x36	//bits 8 to 1 of EEOffset. Bit 0 is bit 7 of register 0x37 
#define EEPROM_Backup_Register			0x37


//BITS IN IMPORTANT REGISTERS

//Bits in Status Register
#define STATUS_EEBUSY	7
#define STATUS_CLKF		6
#define STATUS_BSF		5
#define STATUS_UF		4
#define STATUS_TF		3
#define STATUS_AF		2
#define STATUS_EVF		1
#define STATUS_PORF		0

//Bits in Control1 Register
#define CTRL1_TRPT		7
#define CTRL1_WADA		5//Bit 6 not implemented
#define CTRL1_USEL		4
#define CTRL1_EERD		3
#define CTRL1_TE		2
#define	CTRL1_TD1		1
#define CTRL1_TD0		0

//Bits in Control2 Register
#define CTRL2_TSE		7
#define CTRL2_CLKIE		6
#define CTRL2_UIE		5
#define CTRL2_TIE		4
#define CTRL2_AIE		3
#define CTRL2_EIE		2
#define CTRL2_12_24		1
#define CTRL2_RESET		0

//Bits in Hours register
#define HOURS_AM_PM			5

//Bits in Alarm registers
#define MINUTESALM_AE_M		7
#define HOURSALM_AE_H		7
#define DATE_AE_WD			7

//Commands for EEPROM Command Register (0x27)
#define EEPROMCMD_First					0x00
#define EEPROMCMD_Update				0x11
#define EEPROMCMD_Refresh				0x12
#define EEPROMCMD_WriteSingle			0x21
#define EEPROMCMD_ReadSingle			0x22

//Bits in EEPROM Backup Register
#define EEPROMBackup_TCE_BIT			5				//Trickle Charge Enable Bit
#define EEPROMBackup_FEDE_BIT			4				//Fast Edge Detection Enable Bit (for Backup Switchover Mode)
#define EEPROMBackup_BSM_SHIFT			2				//Backup Switchover Mode shift
#define EEPROMBackup_TCR_SHIFT			0				//Trickle Charge Resistor shift

#define EEPROMBackup_BSM_CLEAR			0b11110011		//Backup Switchover Mode clear
#define EEPROMBackup_TCR_CLEAR			0b11111100		//Trickle Charge Resistor clear
#define	TCR_3K							0b00			//Trickle Charge Resistor 3kOhm
#define	TCR_5K							0b01			//Trickle Charge Resistor 5kOhm
#define	TCR_9K							0b10			//Trickle Charge Resistor 9kOhm
#define	TCR_15K							0b11			//Trickle Charge Resistor 15kOhm


// Clock output register (0x35)
#define EEPROMClkout_CLKOE_BIT			7				//1 = CLKOUT pin is enabled. - Default value on delivery 
#define EEPROMClkout_CLKSY_BIT			6
// Bits 5 and 4 not implemented
#define EEPROMClkout_PORIE				  3				//0 = No interrupt, or canceled, signal on INT pin at POR. - Default value on delivery
																		//1 = An interrupt signal on INT pin at POR. Retained until the PORF flag is cleared to 0 (no automatic cancellation). 
#define EEPROMClkout_FREQ_SHIFT			0				//frequency shift
#define FD_CLKOUT_32k					  0b000			  //32.768 kHz - Default value on delivery 
#define FD_CLKOUT_8192					0b001 			//8192 Hz 
#define FD_CLKOUT_1024					0b010			  //1024 Hz
#define FD_CLKOUT_64					  0b011 		  //64 Hz 
#define FD_CLKOUT_32					  0b100			  //32 Hz
#define FD_CLKOUT_1						  0b101 		 	//1 Hz 
#define FD_CLKOUT_TIMER					0b110			  //Predefined periodic countdown timer interrupt 
#define FD_CLKOUT_LOW					  0b111 			//CLKOUT = LOW 


#define IMT_MASK_CEIE					3				//Clock output when Event Interrupt bit. 
#define IMT_MASK_CAIE					2				//Clock output when Alarm Interrupt bit.
#define IMT_MASK_CTIE					1				//Clock output when Periodic Countdown Timer Interrupt bit.
#define IMT_MASK_CUIE					0				//Clock output when Periodic Time Update Interrupt bit.


#define TIME_ARRAY_LENGTH 7 // Total number of writable values in device

enum time_order {
  TIME_SECONDS,    // 0
  TIME_MINUTES,    // 1
  TIME_HOURS,      // 2
  TIME_WEEKDAY,    // 3
  TIME_DATE,       // 4
  TIME_MONTH,      // 5
  TIME_YEAR,       // 6
};


void RTC_RV3028::begin(TwoWire &wirePort) {
  //We require caller to begin their I2C port, with the speed of their choice
  //external to the library
  //_i2cPort->begin();
  _i2cPort = &wirePort;
  
  delay(1);
  // default is 24hr but set it anyway, just in case
  uint8_t setting = readRegister(RV3028_CTRL2);
  setting &= ~(1 << CTRL2_12_24); //Clear the 12/24 hr bit
  writeRegister(RV3028_CTRL2, setting);
  delay(1);
  // RAM mirror is updated ~66ms after power on reset ... assume it's done
#define EEPB_TCE_BIT  ((uint8_t)0x20)
#define EEPB_FEDE_BIT ((uint8_t)0x10)
#define EEPB_BSM_BITS ((uint8_t)0x0C)
#define EEPB_TCR_BITS ((uint8_t)0x03)
#define EEPB_BSM_DISABLED ((uint8_t)0x00)
#define EEPB_BSM_DSM ((uint8_t)0x04)
#define EEPB_BSM_DISABLED2 ((uint8_t)0x08)
#define EEPB_BSM_LSM ((uint8_t)0x0C)
#define EEPB_TCR_3K ((uint8_t)0x00)
#define EEPB_TCR_5K ((uint8_t)0x01)
#define EEPB_TCR_9K ((uint8_t)0x02)
#define EEPB_TCR_15K ((uint8_t)0x03)
  uint8_t eepBackupReg = readConfigEEPROM_RAMmirror(EEPROM_Backup_Register);
  uint8_t ebrnew = eepBackupReg;
  //  Serial.print("\neepb: ");Serial.println(eepBackupReg,HEX);
  
  // clear bits
  ebrnew &= ~(EEPB_TCE_BIT|EEPB_BSM_BITS|EEPB_TCR_BITS);
  // trickle charge on, fast edge detect, backup switchover direct mode,15K resistor
  // ML414H charging: constant voltage 3.1V, max 30uA, 72hr, internal resistance 600ohm
  // charging pin internally has a .25V Schottky diode in series.
  // charging ckt has 470ohm resistor
  // 3.3V VCC -> 3.05V at trickle charge pin
  // -> 3.05/(15K+470+600) = 189uA
  ebrnew |= EEPB_TCE_BIT|EEPB_FEDE_BIT|EEPB_BSM_DSM|EEPB_TCR_15K;
  if (ebrnew != eepBackupReg) { // write only if changed
    delay(1);
    writeConfigEEPROM_RAMmirror(EEPROM_Backup_Register,ebrnew);
  }
  
  // clear status register
  writeRegister(RV3028_STATUS, 0x00); // do we need this?
}

void RTC_RV3028::adjust(const DateTime& dt)
{
  uint8_t ta[TIME_ARRAY_LENGTH];
  ta[TIME_SECONDS] = bin2bcd(dt.second());
  ta[TIME_MINUTES] = bin2bcd(dt.minute());
  ta[TIME_HOURS] = bin2bcd(dt.hour());
  ta[TIME_WEEKDAY] = dt.dayOfWeek(); // 0-6 so no BCD conversion
  ta[TIME_DATE] = bin2bcd(dt.day());
  ta[TIME_MONTH] = bin2bcd(dt.month());
  ta[TIME_YEAR] = bin2bcd(dt.year() - 2000);
 
  setTime(ta, TIME_ARRAY_LENGTH);
}

DateTime RTC_RV3028::now()
{
  uint8_t ta[TIME_ARRAY_LENGTH]={0};

  readMultipleRegisters(RV3028_SECONDS,ta, TIME_ARRAY_LENGTH);
  return DateTime(bcd2bin(ta[TIME_YEAR]),
                  bcd2bin(ta[TIME_MONTH]),
                  bcd2bin(ta[TIME_DATE]),
                  bcd2bin(ta[TIME_HOURS]),
                  bcd2bin(ta[TIME_MINUTES]),
                  bcd2bin(ta[TIME_SECONDS]));
}

bool RTC_RV3028::setTime(uint8_t * time, uint8_t len)
{
  if (len != TIME_ARRAY_LENGTH)
    return false;
  
  return writeMultipleRegisters(RV3028_SECONDS, time, len);
}

#define CTRL1_EERD_BIT ((uint8_t)0x08)
bool RTC_RV3028::writeConfigEEPROM_RAMmirror(uint8_t eepromaddr, uint8_t val)
{
  bool success = waitforEEPROM();

  //Disable auto refresh by writing 1 to EERD control bit in CTRL1 register
  uint8_t ctrl1 = readRegister(RV3028_CTRL1);
  ctrl1 |= CTRL1_EERD_BIT;
  if (!writeRegister(RV3028_CTRL1, ctrl1)) success = false;
  //Write Configuration RAM Register
  writeRegister(eepromaddr, val);
  //Update EEPROM (All Configuration RAM -> EEPROM)
  writeRegister(RV3028_EEPROM_CMD, EEPROMCMD_First);
  writeRegister(RV3028_EEPROM_CMD, EEPROMCMD_Update);
  if (!waitforEEPROM()) success = false;
  //Reenable auto refresh by writing 0 to EERD control bit in CTRL1 register
  ctrl1 = readRegister(RV3028_CTRL1);
  if (ctrl1 == 0x00)success = false;
  ctrl1 &= ~CTRL1_EERD_BIT;
  writeRegister(RV3028_CTRL1, ctrl1);
  if (!waitforEEPROM()) success = false;
  
  return success;
}

uint8_t RTC_RV3028::readConfigEEPROM_RAMmirror(uint8_t eepromaddr)
{
  bool success = waitforEEPROM();
  
  //Disable auto refresh by writing 1 to EERD control bit in CTRL1 register
  uint8_t ctrl1 = readRegister(RV3028_CTRL1);
  ctrl1 |= CTRL1_EERD_BIT;
  if (!writeRegister(RV3028_CTRL1, ctrl1)) success = false;
  //Read EEPROM Register
  writeRegister(RV3028_EEPROM_ADDR, eepromaddr);
  writeRegister(RV3028_EEPROM_CMD, EEPROMCMD_First);
  writeRegister(RV3028_EEPROM_CMD, EEPROMCMD_ReadSingle);
  if (!waitforEEPROM()) success = false;
  uint8_t eepromdata = readRegister(RV3028_EEPROM_DATA);
  if (!waitforEEPROM()) success = false;
  //Reenable auto refresh by writing 0 to EERD control bit in CTRL1 register
  ctrl1 = readRegister(RV3028_CTRL1);
  if (ctrl1 == 0x00)success = false;
  ctrl1 &= ~CTRL1_EERD_BIT;
  writeRegister(RV3028_CTRL1, ctrl1);
  
  if (!success) return 0xFF;
  return eepromdata;
}

//True if success, false if timeout occured
bool RTC_RV3028::waitforEEPROM()
{
  unsigned long timeout = millis() + 500;
  while ((readRegister(RV3028_STATUS) & 1 << STATUS_EEBUSY) && millis() < timeout);
  
  return millis() < timeout;
}

/*********************************
FOR INTERNAL USE
********************************/

uint8_t RTC_RV3028::readRegister(uint8_t addr)
{
  _i2cPort->beginTransmission(RV3028_ADDR);
  _i2cPort->write(addr);
  _i2cPort->endTransmission();
  
  _i2cPort->requestFrom(RV3028_ADDR, (uint8_t)1);
  if (_i2cPort->available()) {
    return _i2cPort->read();
  }
  else {
    return (0xFF); //Error
  }
}

bool RTC_RV3028::writeRegister(uint8_t addr, uint8_t val)
{
  _i2cPort->beginTransmission(RV3028_ADDR);
  _i2cPort->write(addr);
  _i2cPort->write(val);
  if (_i2cPort->endTransmission() != 0) return (false); //Error: Sensor did not ack
  else return(true);
}

bool RTC_RV3028::readMultipleRegisters(uint8_t addr, uint8_t * dest, uint8_t len)
{
  _i2cPort->beginTransmission(RV3028_ADDR);
  _i2cPort->write(addr);
  if (_i2cPort->endTransmission() != 0)
    return (false); //Error: Sensor did not ack
  
  _i2cPort->requestFrom(RV3028_ADDR, len);
  for (uint8_t i = 0; i < len; i++)
    {
      dest[i] = _i2cPort->read();
    }
  
  return(true);
}

bool RTC_RV3028::writeMultipleRegisters(uint8_t addr, uint8_t * values, uint8_t len)
{
  _i2cPort->beginTransmission(RV3028_ADDR);
  _i2cPort->write(addr);
  for (uint8_t i = 0; i < len; i++)
    {
      _i2cPort->write(values[i]);
    }
  
  if (_i2cPort->endTransmission() != 0)
    return (false); //Error: Sensor did not ack
  return(true);
}

#else // !USE_RV3028 -> DS1307

////////////////////////////////////////////////////////////////////////////////
// RTC_DS1307 implementation

uint8_t RTC_DS1307::begin(void) {
  return 1;
}


uint8_t RTC_DS1307::isrunning(void) {
  Wire.beginTransmission(DS1307_ADDRESS);
  Wire.write(i0);	
  Wire.endTransmission();

  Wire.requestFrom(DS1307_ADDRESS, 1);
  uint8_t ss = Wire.read();
  return !(ss>>7);
}

void RTC_DS1307::adjust(const DateTime& dt) {
    Wire.beginTransmission(DS1307_ADDRESS);
    Wire.write(i0);
    Wire.write(bin2bcd(dt.second()));
    Wire.write(bin2bcd(dt.minute()));
    Wire.write(bin2bcd(dt.hour()));
#ifdef GP2PROTO
    Wire.write(bin2bcd(0x8)); // enable VBATEN bit
#else
    Wire.write(bin2bcd(0));
#endif
    Wire.write(bin2bcd(dt.day()));
    Wire.write(bin2bcd(dt.month()));
    Wire.write(bin2bcd(dt.year() - 2000));
    Wire.write(i0);
    Wire.endTransmission();
}

DateTime RTC_DS1307::now() {
  Wire.beginTransmission(DS1307_ADDRESS);
  Wire.write(i0);	
  Wire.endTransmission();
  
  Wire.requestFrom(DS1307_ADDRESS, 7);
  uint8_t ss = bcd2bin(Wire.read() & 0x7F);
  uint8_t mm = bcd2bin(Wire.read());
  uint8_t hh = bcd2bin(Wire.read());
  Wire.read();
  uint8_t d = bcd2bin(Wire.read());
  uint8_t m = bcd2bin(Wire.read());
  uint16_t y = bcd2bin(Wire.read()) + 2000;
  
  return DateTime (y, m, d, hh, mm, ss);
}

#endif // USE_RV3028
