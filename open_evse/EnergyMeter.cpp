/*
 * Copyright (c) 2020-2022 Sam C. Lin
 */
#include "open_evse.h"
#ifdef KWH_RECORDING
#include "EnergyMeter.h"
#include "J1772EvseController.h"


EnergyMeter g_EnergyMeter;


EnergyMeter::EnergyMeter()
#ifdef PZEM004T
  :
  _pzemSerial(PZEM_RX_PIN,PZEM_TX_PIN),
  _pzem(&_pzemSerial)
#endif // PZEM004T
{
}

void EnergyMeter::init()
{
  m_bFlags = 0;
  m_wattSeconds = 0;

  if (eeprom_read_dword((uint32_t*)EOFS_KWH_ACCUMULATED) == 0xffffffff) { // Check for unitialized eeprom condition so it can begin at 0kWh
    eeprom_write_dword((uint32_t*)EOFS_KWH_ACCUMULATED,0); //  Set the four bytes to zero just once in the case of unitialized eeprom
  }
  
  m_wattHoursTot = eeprom_read_dword((uint32_t*)EOFS_KWH_ACCUMULATED);        // get the stored value for the kWh from eeprom

#ifdef PZEM004T
  _pzemSerial.begin(PZEM_BAUD_RATE);
  _pzem.detectDevice();
#endif
  //DBG
  //  sprintf(g_sTmp,"PZEM: %s",_pzem.deviceDetected() ? "present" : "none");
  //  Serial.println(g_sTmp);
}

#ifdef PZEM004T
int8_t EnergyMeter::readPZEM()
{
  WDT_RESET();
  int rc;
  //  takes 62ms to read meter 10 values
  //  takes 59ms to read meter 9 values
  //  takes 55ms to read meter 7 values
  //  takes 55ms to read meter 1 value
  //      unsigned long cms = millis();
  //        Serial.print("\nrpzem: ");
  rc = _pzem.readMeter();
  //    Serial.println(millis()-cms);
  return rc;
}
#endif // PZEM004T


void EnergyMeter::Update()
{
  WDT_RESET();
  unsigned long curms = millis();
  bool calcusage = 0;
  unsigned long dms = curms - m_lastUpdateMs;
#ifdef PZEM004T
  if (_pzem.deviceDetected()) {
    if (dms > KWH_CALC_INTERVAL_MS) {
      int8_t rc = _pzem.readMeterNonBlocking();
      if (!rc) {
        m_lastUpdateMs = curms;
        calcusage = 1;
      }
      else if (rc < 0) { // error
        // just reset timer
        m_lastUpdateMs = curms;
      }
    }
  }
  else {
#endif //PZEM004T
#ifdef OE_AMMETER
    // n.b. OE ammeter outputs only once every MA_PTS reads
    // because it averages
    if ((g_EvseController.GetState() == EVSE_STATE_C) && !_oeMeter.readAmmeter()) {
      // new value returned
      m_lastUpdateMs = curms;
      calcusage = 1;
      //      Serial.print("ra: ");Serial.println(dms);
    }
#endif // OE_AMMETER
#ifdef PZEM004T
  }
#endif

  // 1. charging session begins when EV is connected, ends when EV disconnected
  // 2. we record only when the relay is closed
  // 3. total kWh updated only when session ends
  uint8_t evconnected = g_EvseController.EvConnected();
  uint8_t relayclosed = g_EvseController.RelayIsClosed();

  if (!evConnected() && evconnected) {
    startSession();
  }
  else if (!evconnected && evConnected()) {
    endSession();
  }

  if (inSession()) {
    if (relayclosed) {
      if (!relayClosed()) {
	// relay just closed - don't calc, just reset timer
	m_lastUpdateMs = millis();
      }
      else {
        if (calcusage) {
	  _calcUsage(dms);
        }
      }
    }

    if (relayclosed) setRelayClosed();
    else clrRelayClosed();
  }

  if (evconnected) setEvConnected();
  else clrEvConnected();
}


void EnergyMeter::_calcUsage(unsigned long dms)
{
#ifdef PZEM004T
  if (pzemDetected()) {
    m_wattSeconds = _pzem.Wh() * 3600ul;
  }
  else {
#endif
#ifdef OE_AMMETER
    uint32_t mv = getVoltage();
    uint32_t ma = _oeMeter.getAvgMA();
    /*
     * The straightforward formula to compute 'milliwatt-seconds' would be:
     *     mws = (mv/1000) * (ma/1000) * dms;
     *
     * However, this has some serious drawbacks, namely, truncating values
     * when using integer math. This can introduce a lot of error!
     *     5900 milliamps -> 5.9 amps (float) -> 5 amps (integer)
     *     0.9 amps difference / 5.9 amps -> 15.2% error
     *
     * The crazy equation below helps ensure our intermediate results always
     * fit in a 32-bit unsigned integer, but retain as much precision as
     * possible throughout the calculation. Here is how it was derived:
     *     mws = (mv/1000) * (ma/1000) * dms;
     *     mws = (mv/(2**3 * 5**3)) * (ma/(2**3 * 5**3)) * dms;
     *     mws = (mv/2**3) * (ma/(2**3) / 5**6 * dms;
     *     mws = (mv/2**4) * (ma/(2**2) / 5**6 * dms;
     *
     * By breaking 1000 into prime factors of 2 and 5, and shifting things
     * around, we almost match the precision of floating-point math.
     *
     * Using 16 and 4 divisors, rather than 8 and 8, helps precision because
     * mv is always greater than ~100000, but ma can be as low as ~6000.
     *
     * A final note- the divisions by factors of 2 are done with right shifts
     * by the compiler, so the revised equation, although it looks quite
     * complex, only requires one divide operation.
     */
    uint32_t mws = (mv/16) * (ma/4) / 15625 * dms;
#ifdef THREEPHASE
    // Multiply calculation by 3 to get 3-phase energy.
    // Typically you'd multiply by sqrt(3), but because voltage is measured to
    // ground (230V) rather than between phases (400 V), 3 is the correct multiple.
    mws *= 3;
#endif // THREEPHASE
    // convert milliwatt-seconds to watt-seconds and increment counter
    m_wattSeconds += mws / 1000;
#endif // OE_AMMETER
#ifdef PZEM004T
  }
#endif
}

void EnergyMeter::startSession()
{
  endSession();
#ifdef PZEM004T
  _pzem.resetEnergy();
#endif //PZEM004T
  m_wattSeconds = 0;
  m_lastUpdateMs = millis();
  setInSession();
}

void EnergyMeter::endSession()
{
  if (inSession()) {
    clrInSession();
    if (m_wattSeconds) {
      m_wattHoursTot += (m_wattSeconds / 3600UL);
      SaveTotkWh();
    }
  }
}

void EnergyMeter::SaveTotkWh()
{
  eeprom_write_dword((uint32_t*)EOFS_KWH_ACCUMULATED,m_wattHoursTot);
}

int8_t EnergyMeter::readAmmeter()
{
  // assumes ammeter is connected after the relay .. only measures current to EV
#ifdef PZEM004T
  if (pzemDetected()) {
    return readPZEM();
  }
  else {
#endif // PZEM004T
#ifdef OE_AMMETER
    return _oeMeter.readAmmeter();
#endif // OE_AMMETER
#ifdef PZEM004T
  }
#endif // PZEM004T
}

uint32_t EnergyMeter::getVoltage()
{
#ifdef PZEM004T
  if (pzemDetected()) return pzemMV();
  else {
#endif
#ifdef OE_AMMETER
    return (uint32_t) (g_EvseController.GetCurSvcLevel() == 1) ? MV_FOR_L1 : MV_FOR_L2;
#endif // OE_AMMETER
#ifdef PZEM004T
  }
#endif
}


int32_t EnergyMeter::getInstantaneousMA()
{
#ifdef PZEM004T
  if (pzemDetected()) return pzemMA();
  else {
#endif
#ifdef OE_AMMETER
    return _oeMeter.getInstantaneousMA();
#endif // OE_AMMETER
#ifdef PZEM004T
  }
#endif
}

int32_t EnergyMeter::getAvgMA()
{
#ifdef PZEM004T
  if (pzemDetected()) {
    return _pzem.mA();
  }
  else {
#endif // PZEM004T
#ifdef OE_AMMETER
    return _oeMeter.getAvgMA();
#endif // OE_AMMETER
#ifdef PZEM004T
  }
#endif // PZEM004T
}
#endif // KWH_RECORDING

