// Code by JeeLabs http://news.jeelabs.org/code/
// Released to the public domain! Enjoy!
/***************************************************************************
RV-3028 support:

Constantin Koch
July 25, 2019
https://github.com/constiko/RV-3028_C7-Arduino_Library

Development environment specifics:
Arduino IDE 1.8.9

This code is released under the [MIT License](http://opensource.org/licenses/MIT).
Please review the LICENSE.md file included with this example. If you have any questions
or concerns with licensing, please contact constantinkoch@outlook.com.
Distributed as-is; no warranty is given.

portions Copyright (c) 2020 Sam C. Lin
******************************************************************************/

#ifndef _RTCLIB_H_
#define _RTCLIB_H_

// Simple general-purpose date/time class (no TZ / DST / leap second handling!)
class DateTime {
public:
    DateTime (uint32_t t =0);
    DateTime (uint16_t year, uint8_t month, uint8_t day,
                uint8_t hour =0, uint8_t min =0, uint8_t sec =0);
    DateTime (const char* date, const char* time);
    uint16_t year() const       { return 2000 + yOff; }
    uint8_t month() const       { return m; }
    uint8_t day() const         { return d; }
    uint8_t hour() const        { return hh; }
    uint8_t minute() const      { return mm; }
    uint8_t second() const      { return ss; }
    uint8_t dayOfWeek() const;

    // 32-bit times as seconds since 1/1/2000
    long secondstime() const;   
    // 32-bit times as seconds since 1/1/1970
    uint32_t unixtime(void) const;

protected:
    uint8_t yOff, m, d, hh, mm, ss;
};

// RTC based on the DS1307 chip connected via I2C and the Wire library
class RTC_DS1307 {
public:
  static uint8_t begin(void);
    static void adjust(const DateTime& dt);
    uint8_t isrunning(void);
    static DateTime now();
};

class RTC_RV3028 {
  TwoWire *_i2cPort;

  uint8_t readRegister(uint8_t addr);
  bool writeRegister(uint8_t addr, uint8_t val);
  bool readMultipleRegisters(uint8_t addr, uint8_t * dest, uint8_t len);
  bool writeMultipleRegisters(uint8_t addr, uint8_t * values, uint8_t len);
  bool setTime(uint8_t * time, uint8_t len);

public:
  void begin(TwoWire &wirePort = Wire);
  void adjust(const DateTime& dt);
  DateTime now();

  bool writeConfigEEPROM_RAMmirror(uint8_t eepromaddr, uint8_t val);
  uint8_t readConfigEEPROM_RAMmirror(uint8_t eepromaddr);
  bool waitforEEPROM();
};

#endif // _RTCLIB_H_
