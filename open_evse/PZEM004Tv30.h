#ifdef PZEM004T
/*
 Copyright (c) 2021 Sam C. Lin
Copyright (c) 2019 Jakub Mandula

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/


/*
 * PZEM-004Tv30.h
 *
 * Interface library for the upgraded version of PZEM-004T v3.0
 * Based on the PZEM004T library by @olehs https://github.com/olehs/PZEM004T
 *
 * Author: Jakub Mandula https://github.com/mandulaj
 *
 *
*/

  // 9600 baud: tested w/SoftSerial on AtMega328P
    //  takes 62ms to read meter 10 values
    //  takes 59ms to read meter 9 values
    //  takes 55ms to read meter 7 values
    //  takes 38ms to read meter 1 value
#ifndef PZEM004TV30_H
#define PZEM004TV30_H

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#define PZEM_BAUD_RATE 9600
#define PZEM_DEFAULT_ADDR    0xF8

#ifdef PZEM_ALARM
#define PZEM_READ_BYTE_CNT 25
#define PZEM_READ_REGISTER_CNT 10
#else
#define PZEM_READ_BYTE_CNT 23
#define PZEM_READ_REGISTER_CNT 9
#endif

class PZEM004Tv30
{
private:
  bool _deviceDetected;
  uint8_t _responseBuf[PZEM_READ_BYTE_CNT];
  Stream* _serial; // Serial interface
  
  uint8_t _addr;   // Device address
  unsigned long _waitStart;
  uint8_t _bytesRead;
  enum { PZEM_IDLE,PZEM_WAIT } _readState = PZEM_IDLE;
  struct {
    uint32_t mV;
    uint32_t mA;
    uint32_t mW;
    uint32_t Wh; // Wh
    uint16_t frequency; // Hz*10
    uint16_t pf; // pf*100
#ifdef PZEM_ALARM
    uint16_t alarms;
#endif // PZEM_ALARM
  }  _currentValues; // Measured values
  
  void updateCurVals(uint8_t *response);
  void zeroCurVals();
  
  void init(uint8_t addr); // Init common to all constructors
  
  uint16_t receive(uint8_t *resp, uint16_t len); // Receive len bytes into a buffer
  
  bool sendCmd8(uint8_t cmd, uint16_t rAddr, uint16_t val, uint16_t slave_addr=0xFFFF); // Send 8 byte command
  
  void setCRC(uint8_t *buf, uint16_t len);           // Set the CRC for a buffer
  bool checkCRC(const uint8_t *buf, uint16_t len);   // Check CRC of buffer
  
  uint16_t CRC16(const uint8_t *data, uint16_t len); // Calculate CRC of buffer
 public:
  PZEM004Tv30(Stream *port, uint8_t addr=PZEM_DEFAULT_ADDR);
  ~PZEM004Tv30();
  
  
  bool deviceDetected() { return _deviceDetected; }
  bool detectDevice();

  uint32_t mV() { return _currentValues.mV; }
  uint32_t mA() { return _currentValues.mA; }
  uint32_t mW() { return _currentValues.mW; }
  uint32_t Wh() { return _currentValues.Wh; }
  uint16_t freq() { return _currentValues.frequency; } // Hz*10
  uint16_t pf() { return _currentValues.pf; } // pf*100
  
  
  bool setAddress(uint8_t addr);
  uint8_t getAddress();
  
#ifdef PZEM_ALARM
  bool setPowerAlarm(uint16_t watts);
  bool getPowerAlarm();
#endif // PZEM_ALARM
  
  int8_t readMeter();    // Get most up to date values from device registers and cache them
  int8_t readMeterNonBlocking();
  bool resetEnergy();
  
  void search();
  
};

#endif // PZEM004T_H
#endif // PZEM004T
