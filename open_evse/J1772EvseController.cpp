/*
 * This file is part of Open EVSE.
 *
 * Copyright (c) 2011-2022 Sam C. Lin
 *
 * Open EVSE is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.

 * Open EVSE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Open EVSE; see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include "open_evse.h"
#include "EnergyMeter.h"

#ifdef FT_ENDURANCE
int g_CycleCnt = -1;
long g_CycleHalfStart;
uint8_t g_CycleState;
#endif 

//                                               A/B B/C C/D D DS
THRESH_DATA J1772EVSEController::m_ThreshData = {875,780,690,0,260};

//#define THRESH_PILOT_FAIL 200 failed for -11.74V
#define OE2_THRESH_PILOT_FAIL 220 // random adjustment w/o testing to try to pass -11.74
#define OE1_THRESH_PILOT_FAIL 590

J1772EVSEController g_EvseController;

J1772EVSEController::J1772EVSEController() :
  adcPilot(PILOT_PIN)
{
#ifdef STATE_TRANSITION_REQ_FUNC
  m_StateTransitionReqFunc = NULL;
#endif // STATE_TRANSITION_REQ_FUNC
}

void J1772EVSEController::SaveSettings()
{
  uint8_t *dest;
  // n.b. should we add dirty bits so we only write the changed values? or should we just write them on the fly when necessary?
  if (GetCurSvcLevel() == 1) {
    dest = (uint8_t *)EOFS_CURRENT_CAPACITY_L1;
  }
  else {
    dest = (uint8_t *)EOFS_CURRENT_CAPACITY_L2;
  }
  eeprom_write_byte(dest, GetCurrentCapacity());
  SaveEvseFlags();
}


#ifdef AUTH_LOCK
void J1772EVSEController::AuthLock(uint8_t tf,uint8_t update)
{
  if (tf) {
    setVFlags(ECVF_AUTH_LOCKED);
  }
  else {
    clrVFlags(ECVF_AUTH_LOCKED);
  }

  if (update && !InFaultState()) {
    Update(1);
    g_OBD.Update(OBD_UPD_FORCE);
  }
}
#endif // AUTH_LOCK


// use watchdog to perform a reset
void J1772EVSEController::Reboot()
{
  setPilotState(PILOT_STATE_P12);

#ifdef LCD16X2
  g_OBD.LcdPrint_P(1,g_psResetting);
#endif

  if (chargingIsOn()) {
   // give the EV some time to open its contactor in response to P12
    wdt_delay(2000);
  }

  // hardware reset by forcing watchdog to timeout
  wdt_enable(WDTO_1S);   // enable watchdog timer
  delay(1500);
}



#ifdef SHOW_DISABLED_TESTS
void J1772EVSEController::DisabledTest_P(PGM_P message)
{
#ifdef LCD16X2
  g_OBD.LcdMsg_P(g_psDisabledTests, message);
#endif
#ifndef NOCHECKS
  wdt_delay(SHOW_DISABLED_DELAY);
#endif
}

void J1772EVSEController::ShowDisabledTests()
{
  if (m_wFlags & (ECF_DIODE_CHK_DISABLED|
		  ECF_VENT_REQ_DISABLED|
		  ECF_GND_CHK_DISABLED|
		  ECF_STUCK_RELAY_CHK_DISABLED|
		  ECF_GFI_TEST_DISABLED|
                  ECF_TEMP_CHK_DISABLED)) {
#ifdef LCD16X2
    g_OBD.LcdSetBacklightColor(YELLOW);
#endif // #ifdef LCD16X2

    if (!DiodeCheckEnabled()) {
      DisabledTest_P(g_psDiodeCheck);
    }
    if (!VentReqEnabled()) {
      DisabledTest_P(g_psVentReqChk);
    }
#ifdef ADVPWR
    if (!GndChkEnabled()) {
      DisabledTest_P(g_psGndChk);
    }
    if (!StuckRelayChkEnabled()) {
      DisabledTest_P(g_psRlyChk);
    }
#endif // ADVPWR
#ifdef GFI_SELFTEST
    if (!GfiSelfTestEnabled()) {
      DisabledTest_P(g_psGfiTest);
    }
#endif // GFI_SELFTEST
#ifdef TEMPERATURE_MONITORING
    if (!TempChkEnabled()) {
      DisabledTest_P(g_psTempChk);
    }
#endif // TEMPERATURE_MONITORING

#ifdef LCD16X2
    g_OBD.LcdSetBacklightColor(WHITE);
#endif
  }
}
#endif //SHOW_DISABLED_TESTS

void J1772EVSEController::chargingOn()
{
  // turn on charging current
#ifdef RELAY_AUTO_PWM_PIN
  // turn on charging pin to close relay
  digitalWrite(RELAY_AUTO_PWM_PIN,HIGH);
  wdt_delay(m_relayCloseMs);
  // switch to PWM to hold closed
  analogWrite(RELAY_AUTO_PWM_PIN,m_relayHoldPwm);
#else // !RELAY_AUTO_PWM_PIN
  pinCharging.write(1);

  if (isOE1()) {
    pinCharging2.write(1);
    pinChargingAC.write(1);
  }
#endif // RELAY_AUTO_PWM_PIN

  setVFlags(ECVF_CHARGING_ON);
  
  if (vFlagIsSet(ECVF_SESSION_ENDED)) {
    m_AccumulatedChargeTime = 0;
    clrVFlags(ECVF_SESSION_ENDED);
  }
  else {
    m_AccumulatedChargeTime += m_ElapsedChargeTime;
  }

  m_ChargeOnTime = now();
  m_ChargeOnTimeMS = millis();
}

void J1772EVSEController::chargingOff()
{
 // turn off charging current
#ifdef RELAY_AUTO_PWM_PIN
  digitalWrite(RELAY_AUTO_PWM_PIN,LOW);
#else // !RELAY_AUTO_PWM_PIN

  pinCharging.write(0);
  if (isOE1()) {
    pinCharging2.write(0);
    pinChargingAC.write(0);
  }
#endif // RELAY_AUTO_PWM_PIN

  clrVFlags(ECVF_CHARGING_ON);

  m_ChargeOffTime = now();
  m_ChargeOffTimeMS = millis();

#ifdef AMMETER
  m_ChargingCurrent = 0;
  m_LastCCDirMa = 0;
  m_ChargingCurrentDir = 0;
#endif
} 

void J1772EVSEController::HardFault()
{
  SetHardFault();
  g_OBD.Update(OBD_UPD_HARDFAULT);
#ifdef RAPI
  RapiSendEvseState();
#endif
#ifdef MENNEKES_LOCK
  m_MennekesLock.Unlock();
#endif // MENNEKES_LOCK
  while (1) {
    ProcessInputs(); // spin forever or until user resets via menu
    // if pilot not in N12 state, we can recover from the hard fault when EV
    // is unplugged
    if (m_Pilot.GetState() != PILOT_STATE_N12) {
      ReadPilot(); // update EV connect state
      if (!EvConnected()) {
	// EV disconnected - cancel fault
	m_EvseState = EVSE_STATE_UNKNOWN;
	break;
      }
    }
  }
  ClrHardFault();
}

#ifdef GFI
void J1772EVSEController::_logGfiSelfTestFail()
{
  if (((uint8_t)(m_GfiSelfTestFailCnt+1)) < 254) {
    m_GfiSelfTestFailCnt++;
    eeprom_write_byte((uint8_t*)EOFS_GFI_SELF_TEST_FAIL_CNT,m_GfiSelfTestFailCnt);
  }
}


void J1772EVSEController::SetGfiTripped()
{
#ifdef GFI_SELFTEST
  if (m_Gfi.SelfTestInProgress()) {
    m_Gfi.SetTestSuccess();
    return;
  }
#endif
  setVFlags(ECVF_GFI_TRIPPED);

  // this is repeated in Update(), but we want to keep latency as low as possible
  // for safety so we do it here first anyway
  chargingOff(); // turn off charging current
  // turn off the PWM
  setPilotState(PILOT_STATE_P12);

  m_Gfi.SetFault();
  // the rest of the logic will be handled in Update()
}
#endif // GFI

void J1772EVSEController::EnableDiodeCheck(uint8_t tf)
{
  if (tf) {
    m_wFlags &= ~ECF_DIODE_CHK_DISABLED;
  }
  else {
    m_wFlags |= ECF_DIODE_CHK_DISABLED;
  }
  SaveEvseFlags();
}

#ifdef AUTH_LOCK
void J1772EVSEController::AutoAuthLockEnable(uint8_t tf)
{
  if (tf) {
    setFlags(ECF_AUTO_AUTH_LOCK);
    AuthLock(1,1); // lock it
  }
  else clrFlags(ECF_AUTO_AUTH_LOCK);
  SaveEvseFlags();
}
#endif // AUTH_LOCK


#ifdef GFI_SELFTEST
void J1772EVSEController::EnableGfiSelfTest(uint8_t tf)
{
  if (tf) {
    m_wFlags &= ~ECF_GFI_TEST_DISABLED;
  }
  else {
    m_wFlags |= ECF_GFI_TEST_DISABLED;
  }
  SaveEvseFlags();
}
#endif

#ifdef TEMPERATURE_MONITORING
void J1772EVSEController::EnableTempChk(uint8_t tf)
{
  if (tf) {
    m_wFlags &= ~ECF_TEMP_CHK_DISABLED;
  }
  else {
    m_wFlags |= ECF_TEMP_CHK_DISABLED;
  }
  SaveEvseFlags();
}
#endif // TEMPERATURE_MONITORING

void J1772EVSEController::EnableVentReq(uint8_t tf)
{
  if (tf) {
    m_wFlags &= ~ECF_VENT_REQ_DISABLED;
  }
  else {
    m_wFlags |= ECF_VENT_REQ_DISABLED;
  }
  SaveEvseFlags();
}

#ifdef ADVPWR
void J1772EVSEController::EnableGndChk(uint8_t tf)
{
  if (tf) {
    m_wFlags &= ~ECF_GND_CHK_DISABLED;
  }
  else {
    m_NoGndRetryCnt = 0;
    m_NoGndStart = 0;
    m_wFlags |= ECF_GND_CHK_DISABLED;
  }
  SaveEvseFlags();
}

void J1772EVSEController::EnableStuckRelayChk(uint8_t tf)
{
  if (tf) {
    m_wFlags &= ~ECF_STUCK_RELAY_CHK_DISABLED;
  }
  else {
    m_wFlags |= ECF_STUCK_RELAY_CHK_DISABLED;
  }
  SaveEvseFlags();
}

#ifdef AUTOSVCLEVEL
void J1772EVSEController::EnableAutoSvcLevel(uint8_t tf)
{
  if (tf) {
    m_wFlags &= ~ECF_AUTO_SVC_LEVEL_DISABLED;
  }
  else {
    m_wFlags |= ECF_AUTO_SVC_LEVEL_DISABLED;
  }
  SaveEvseFlags();
}
#endif // AUTOSVCLEVEL



#endif // ADVPWR

#ifdef RGBLCD
int J1772EVSEController::SetBacklightType(uint8_t t,uint8_t update)
{
  g_OBD.LcdSetBacklightType(t,update);
  if (t == BKL_TYPE_MONO) m_wFlags |= ECF_MONO_LCD;
  else m_wFlags &= ~ECF_MONO_LCD;
  SaveEvseFlags();
  return 0;
}
#endif // RGBLCD
void J1772EVSEController::Enable()
{
  if ((m_EvseState == EVSE_STATE_DISABLED)||
      (m_EvseState == EVSE_STATE_SLEEPING)) {
#ifdef SLEEP_STATUS_REG
    if (m_EvseState == EVSE_STATE_SLEEPING) {
      pinSleepStatus.write(0);
    }
#endif // SLEEP_STATUS_REG
#if defined(TIME_LIMIT) || defined(CHARGE_LIMIT)
	SetLimitSleep(0);
#endif //defined(TIME_LIMIT) || defined(CHARGE_LIMIT)
    
    m_PrevEvseState = EVSE_STATE_DISABLED;
    m_EvseState = EVSE_STATE_UNKNOWN;
    setPilotState(PILOT_STATE_P12);

#ifdef EGOLF_KLUDGE
    if (EGolfKludgeIsEnabled()) {
      ReadPilot(); // update EV connect state
      if (EvConnected()) {
        /* e-Golf Modification */
        m_Pilot.SetState(PILOT_STATE_N12);
        wdt_delay(1000); // Pause
        m_Pilot.SetState(PILOT_STATE_P12);
        wdt_delay(1000); // Pause
        m_Pilot.SetState(PILOT_STATE_N12);
        wdt_delay(1000); // Pause
        m_Pilot.SetState(PILOT_STATE_P12);
      }
    }
#endif // EGOLF_KLUDGE
  }
}

void J1772EVSEController::Disable()
{
  if (m_EvseState != EVSE_STATE_DISABLED) { 
    setPilotState(PILOT_STATE_P12);
    m_EvseState = EVSE_STATE_DISABLED;
    // panic stop so we won't wait for EV to open its contacts first
    chargingOff();
    g_OBD.Update(OBD_UPD_FORCE);
#ifdef RAPI
    RapiSendEvseState();
#endif // RAPI
  }
}


void J1772EVSEController::Sleep()
{
  if (m_EvseState != EVSE_STATE_SLEEPING) {
    setPilotState(PILOT_STATE_P12);
    m_EvseState = EVSE_STATE_SLEEPING;
#ifdef SLEEP_STATUS_REG
    pinSleepStatus.write(1);
#endif // SLEEP_STATUS_REG

    // try to prevent arcing of our relay by waiting for EV to open its contacts first
    // use the charge end time variable temporarily to count down
    // when to open the contacts in Update()
    m_ChargeOffTimeMS = millis();

    g_OBD.Update(OBD_UPD_FORCE);

#ifdef RAPI
    RapiSendEvseState();
#endif // RAPI
  }
}

void J1772EVSEController::SetSvcLevel(uint8_t svclvl,uint8_t updatelcd)
{
  if (svclvl == 2) {
    m_wFlags |= ECF_L2; // set to Level 2
  }
  else {
    svclvl = 1; // force invalid value to L1
    m_wFlags &= ~ECF_L2; // set to Level 1
  }

  SaveEvseFlags();

  uint8_t ampacity = GetMaxCurrentCapacity();

  SetCurrentCapacity(ampacity,0,1);

  if (updatelcd) {
    g_OBD.Update(OBD_UPD_FORCE);
  }
}

uint8_t J1772EVSEController::GetMaxCurrentCapacity()
{
  uint8_t svclvl = GetCurSvcLevel();
  uint8_t ampacity =  eeprom_read_byte((uint8_t*)((svclvl == 1) ? EOFS_CURRENT_CAPACITY_L1 : EOFS_CURRENT_CAPACITY_L2));

  if ((ampacity == 0xff) || (ampacity == 0)) {
    ampacity = (svclvl == 1) ? DEFAULT_CURRENT_CAPACITY_L1 : DEFAULT_CURRENT_CAPACITY_L2;
  }
  
  if (ampacity < MIN_CURRENT_CAPACITY_J1772) {
    ampacity = MIN_CURRENT_CAPACITY_J1772;
  }
  else {
    if (svclvl == 1) { // L1
      if (ampacity > MAX_CURRENT_CAPACITY_L1) {
        ampacity = MAX_CURRENT_CAPACITY_L1;
      }
    }
    else {
      if (ampacity > MAX_CURRENT_CAPACITY_L2) {
        ampacity = MAX_CURRENT_CAPACITY_L2;
      }
    }
  }

  return ampacity;
}

#ifdef ADVPWR

// acpinstate : bit 1 = AC pin 1, bit0 = AC pin 2
uint8_t J1772EVSEController::ReadACPins()
{
  if (isOE1()) {
    // for rectified MID400 chips which block
    // half cycle (for ground check on both legs)
    //
    // AC pins are active low, so we set them high
    // and then if voltage is detected on a pin, it will go low
    //
    uint8_t ac1 = 2;
    uint8_t ac2 = 1;
    unsigned long startms = millis();
    
    do {
      if (ac1 && !pinAC1.read()) {
        ac1 = 0;
      }
      if (ac2 && !pinAC2.read()) {
        ac2 = 0;
      }
    } while ((ac1 || ac2) && ((millis() - startms) < AC_SAMPLE_MS));
    return ac1 | ac2;
  }
#ifdef OPENEVSE_2
  else { // OE2
    // For OpenEVSE II, there is only ACLINE1_PIN, and it is
    // active *high*. '3' is the value for "both AC lines dead"
    // and '0' is the value for "both AC lines live". There is
    // no need to sample, as the hardware does a peak-hold.
    return (pinAC1.read() ? 0 : 3);
  }
#endif // OPENEVSE_2
}


// if autosvclevel == NULL, then run all tests, regardless of whether or not
// they are enabled
uint8_t J1772EVSEController::doPost(uint8_t *autosvclevel)
{
  uint8_t postcode = PS;

  WDT_RESET();

  if (!isOE1()) { // OE2
#ifdef GFI_SELFTEST
    // only run GFI test if no fault detected above
    if (!autosvclevel || GfiSelfTestEnabled()) {
      if (m_Gfi.SelfTest()) {
        _logGfiSelfTestFail();
        postcode = FG;
        goto bye;
      }
    }
#endif // GFI_SELFTEST
    
    if (!autosvclevel) {
      // check to see if pilot can go to N12
      setPilotState(PILOT_STATE_N12);
      wdt_delay(150); // delay reading for stable pilot before reading
      if (adcPilot.read() > ((isOE1() ? OE1_THRESH_PILOT_FAIL : OE2_THRESH_PILOT_FAIL))) {
        postcode = PE;
        goto bye;
      }
    }
    
    setPilotState(PILOT_STATE_P12);
    
#ifdef AUTOSVCLEVEL
    if (autosvclevel && AutoSvcLevelEnabled()) {
#ifdef PZEM004T
      if (g_EnergyMeter.pzemDetected()) {
        // L1L2 autodetect use PZEM instead of OE2 voltmeter
        g_EnergyMeter.readPZEM();
        if (g_EnergyMeter.pzemMV() > 180000) { // 180V
          *autosvclevel = L2;
        } else {
          *autosvclevel = L1;
        }
      }
      else {
#endif // PZEM004T
#ifdef OE_AMMETER
        // For OpenEVSE II, there is a voltmeter for auto L1/L2.
        if (g_EnergyMeter.getOEEnergyMeter()->readOe2Voltmeter() > OE2_L2_VOLT_ADC_THRESHOLD) {
          *autosvclevel = L2;
        } else {
          *autosvclevel = L1;
        }
#endif //OE_AMMETER
#ifdef PZEM004T
      }
#endif // PZEM004T
    }
#endif // AUTOSVCLEVEL

#ifdef GFI_SELFTEST
    // close relay to test - we won't run it unless we passed GFI self test
    // because there's a shock hazard closing the relay when EV not connected
    // but we only run it when EV not connected, because contactor isn't supposed
    // to close unless the EV requests charge
    if (!autosvclevel) { // always run it for forced self test //  && (adcPilot.read() >= m_ThreshData.m_ThreshAB)) { // !EV connected
      // IF EV is not connected it's OK to close the relay to test
      pinCharging.write(1);
      wdt_delay(RelaySettlingTime);
      if (ReadACPins() != 0) {
        // either relay didn't close, or we can't detect that it closed
        // or no ground
        // since the assumption in Update() is it means NO GROUND, set OG error
        postcode = OG;
      }
      pinCharging.write(0);
      wdt_delay(RelaySettlingTime);
    }
#endif // GFI_SELFTEST    
    if ((postcode == PS) && (!autosvclevel || StuckRelayChkEnabled())) {
      // relay should be open
      if (ReadACPins() == 0) {
        // relay is stuck shut or sense pin stuck HIGH
        postcode = SR;
      }
    }
  } 
  else { // !OE2
    uint8_t svcState = PS;
#ifdef AUTOSVCLEVEL
    if (AutoSvcLevelEnabled() || !autosvclevel) {
      uint8_t RelayOff;
      uint8_t Relay1, Relay2; //Relay Power status
      
      setPilotState(PILOT_STATE_P12); //check to see if EV is plugged in
      wdt_delay(150); // delay reading for stable pilot before reading
      uint16_t reading = adcPilot.read(); //read pilot
      setPilotState(PILOT_STATE_N12);
      if (reading >= m_ThreshData.m_ThreshAB) {  // IF EV is not connected its Okay to open the relay the do the L1/L2 and ground Check
        
        // save state with both relays off - for stuck relay state
        RelayOff = ReadACPins();
        
        // save state with Relay 1 on 
        pinCharging.write(1);
        pinChargingAC.write(1);
        wdt_delay(RelaySettlingTime);

        Relay1 = ReadACPins();

        pinCharging.write(0);
        pinChargingAC.write(0);
        wdt_delay(RelaySettlingTime); //allow relay to fully open before running other tests
          
        // save state for Relay 2 on
        pinCharging2.write(1); 
        wdt_delay(RelaySettlingTime);
        Relay2 = ReadACPins();
        pinCharging2.write(0); 
        wdt_delay(RelaySettlingTime); //allow relay to fully open before running other tests
        
        // decide input power state based on the status read  on L1 and L2
        // either 2 SPST or 1 DPST relays can be configured 
        // valid svcState is L1 - one hot, L2 both hot, OG - open ground both off, SR - stuck relay when shld be off 
        //  
        if (RelayOff == none) { // relay not stuck on when off
          switch ( Relay1 ) {
          case ( both ): //
            if ( Relay2 == none ) svcState = L2;
            if (StuckRelayChkEnabled()) {
              if ( Relay2 != none ) svcState = SR;
            }
            break;
          case ( none ): //
            if (GndChkEnabled()) {
              if ( Relay2 == none ) svcState = OG;
            }
            if ( Relay2 == both ) svcState = L2;
            if ( Relay2 == L1 || Relay2 == L2 ) svcState = L1;
            break;
          case ( L1on ): // L1 or L2
          case ( L2on ):
            if (StuckRelayChkEnabled()) {
              if ( Relay2 != none ) svcState = SR;
            }
          if ( Relay2 == none ) svcState = L1;
          if ( (Relay1 == L1on) && (Relay2 == L2on)) svcState = L2;
          if ( (Relay1 == L2on) && (Relay2 == L1on)) svcState = L2;
          break;
          } // end switch
        }
        else { // Relay stuck on
          if (StuckRelayChkEnabled()) {
            svcState = SR;
          }
        }
      } // endif test, no EV is plugged in
      else {
        // since we can't auto detect, for safety's sake, we must set to L1
        svcState = L1;
        SetAutoSvcLvlSkipped(1);
        // EV connected.. do stuck relay check
      }
      
      if (autosvclevel && ((svcState == L1) || (svcState == L2))) {
        *autosvclevel = svcState;
      }
    }
#endif // AUTOSVCLEVEL
    if ((svcState <= L2) && StuckRelayChkEnabled()) {
      if ((ReadACPins() & 3) != 3) {
        svcState = SR;
      }
    }
    
#ifdef GFI_SELFTEST
    // only run GFI test if no fault detected above
    if ((svcState <= L2) && (!autosvclevel || GfiSelfTestEnabled())) {
      if (m_Gfi.SelfTest()) {
        _logGfiSelfTestFail();
        svcState = FG;
      }
    }
#endif
    if (svcState > L2) postcode = svcState;
  } // !OE2


 bye:
  if (postcode == PS) {
    if (autosvclevel && AutoSvcLevelEnabled()) {
#ifdef LCD16X2
      g_OBD.LcdMsg_P(g_psAutoDetect,(*autosvclevel == L1) ? g_psLevel1 : g_psLevel2);
      wdt_delay(500);
#endif // LCD16X2
    }
    g_OBD.SetRedLed(0);
    setPilotState(PILOT_STATE_P12);
  }
  else { // error
#ifdef LCD16X2
    const char *pserr;
#endif 
    if (postcode == OG) {
      m_EvseState = EVSE_STATE_NO_GROUND;
#ifdef LCD16X2
      pserr = g_psNoGround;
#endif
    }
    else if (postcode == SR) {
      m_EvseState = EVSE_STATE_STUCK_RELAY;
#ifdef LCD16X2
      pserr = g_psStuckRelay;
#endif
    }
#ifdef GFI_SELFTEST
    else if (postcode == FG) {
      m_EvseState = EVSE_STATE_GFI_TEST_FAILED;
#ifdef LCD16X2
      pserr = g_psGfiTest;
#endif
    }
#endif // GFI_SELFTEST
    else {
      m_EvseState = EVSE_STATE_PILOT_ERROR;
#ifdef LCD16X2
      pserr = g_psPilot;
#endif
    }
#ifdef LCD16X2
    g_OBD.LcdMsg_P(g_psTestFailed,pserr);
    g_OBD.LcdSetBacklightColor(RED);
#endif // LCD16X2
    g_OBD.SetGreenLed(0);
    g_OBD.SetRedLed(1);
    setPilotState(PILOT_STATE_N12);
  }

  WDT_RESET();

  return postcode;
}
#endif // ADVPWR

void J1772EVSEController::Init()
{
#ifdef MENNEKES_LOCK
  m_MennekesLock.Init();
#endif // MENNEKES_LOCK

  m_EvseState = EVSE_STATE_UNKNOWN;
  m_PrevEvseState = EVSE_STATE_UNKNOWN;

  // read settings from EEPROM
  uint16_t rflgs = eeprom_read_word((uint16_t*)EOFS_FLAGS);

#ifdef RGBLCD
  if ((rflgs != 0xffff) && (rflgs & ECF_MONO_LCD)) {
    g_OBD.LcdSetBacklightType(BKL_TYPE_MONO);
  }
#endif // RGBLCD

#ifdef RELAY_AUTO_PWM_PIN_TESTING
  m_relayHoldPwm = eeprom_read_byte((uint8_t*)EOFS_RELAY_HOLD_PWM);
  m_relayCloseMs = eeprom_read_dword((uint32_t*)EOFS_RELAY_CLOSE_MS);
  if (m_relayCloseMs > 5000) {
    m_relayCloseMs = 0;
    m_relayHoldPwm = 248;
  }
  Serial.print("\nrelayCloseMs: ");Serial.println(m_relayCloseMs);
  Serial.print("relayHoldPwm: ");Serial.println(m_relayHoldPwm);
#endif


#ifdef RELAY_AUTO_PWM_PIN
  pinMode(RELAY_AUTO_PWM_PIN,OUTPUT);
#else // !RELAY_AUTO_PWM_PIN
#endif // RELAY_AUTO_PWM_PIN

#ifdef ACLINE1_REG
  pinAC1.init(ACLINE1_REG,ACLINE1_IDX,DigitalPin::INP_PU);
#endif
#ifdef SLEEP_STATUS_REG
  pinSleepStatus.init(SLEEP_STATUS_REG,SLEEP_STATUS_IDX,DigitalPin::OUT);
#endif
#ifdef AUTH_LOCK_REG
  pinAuthLock.init(AUTH_LOCK_REG,AUTH_LOCK_IDX,DigitalPin::INP_PU);
#endif

#ifdef GFI
  m_Gfi.Init();
#endif // GFI

  chargingOff();

  m_Pilot.Init(); // init the pilot

  uint8_t svclvl = (uint8_t)DEFAULT_SERVICE_LEVEL;

  if (rflgs == 0xffff) { // uninitialized EEPROM
    m_wFlags = ECF_DEFAULT;
#ifdef RGBLCD
    if (DEFAULT_LCD_BKL_TYPE == BKL_TYPE_MONO) {
      m_wFlags |= ECF_MONO_LCD;
    }
#endif // RGBLCD
  }
  else {
    m_wFlags = rflgs;
    svclvl = GetCurSvcLevel();
  }


#ifdef OE2_VOLTMETER_PIN
  // detect OE1 or OE2
  {
    uint16_t adclo,adchi;
    g_EnergyMeter.getOEEnergyMeter()->readOe2Voltmeter(&adchi,&adclo);
    // OE2 voltmeter pin = PP pin on OE1
    // raw adc lo: ~0
    // raw adc hi: ~288= 120v, ~763 = 240V
    // if used as PP, value should be unchanging, so adclo ~adchi
    if ((adchi-adclo) < 50) {
      m_wFlags |= ECF_OE1_COMPATIBLE;
    }
  }
#endif // OE2_VOLTMETER_PIN
  if (isOE1()) { // OE1
    pinCharging.init(OE1_CHARGING_REG,OE1_CHARGING_IDX,DigitalPin::OUT);
    pinCharging2.init(OE1_CHARGING2_REG,OE1_CHARGING2_IDX,DigitalPin::OUT);
    pinChargingAC.init(OE1_CHARGINGAC_REG,OE1_CHARGINGAC_IDX,DigitalPin::OUT);
    pinAC2.init(ACLINE2_REG,ACLINE2_IDX,DigitalPin::INP_PU);
  }
  else { // OE2
    pinCharging.init(OE2_CHARGING_REG,OE2_CHARGING_IDX,DigitalPin::OUT);
  }
      
#ifdef NOCHECKS
  m_wFlags |= ECF_DIODE_CHK_DISABLED|ECF_VENT_REQ_DISABLED|ECF_GND_CHK_DISABLED|ECF_STUCK_RELAY_CHK_DISABLED|ECF_GFI_TEST_DISABLED|ECF_TEMP_CHK_DISABLED;
#endif

#ifdef AMMETER
#ifdef OE_AMMETER
  {
    int16_t ammeterCurrentOffset = eeprom_read_word((uint16_t*)EOFS_AMMETER_CURR_OFFSET);
    int16_t currentScaleFactor = eeprom_read_word((uint16_t*)EOFS_CURRENT_SCALE_FACTOR);
  
    if (ammeterCurrentOffset == (int16_t)0xffff) {
      ammeterCurrentOffset = isOE1() ? DEFAULT_AMMETER_CURRENT_OFFSET_OE1 : DEFAULT_AMMETER_CURRENT_OFFSET_OE2;
    }
    if (currentScaleFactor == (int16_t)0xffff) {
      currentScaleFactor = isOE1() ? DEFAULT_CURRENT_SCALE_FACTOR_OE1 : DEFAULT_CURRENT_SCALE_FACTOR_OE2;
    }
    g_EnergyMeter.getOEEnergyMeter()->setAmmeterCalibration(currentScaleFactor,ammeterCurrentOffset);
  }
  #endif // OE_AMMETER
  m_ChargingCurrent = 0;
#ifdef OVERCURRENT_THRESHOLD
  m_OverCurrentStartMs = 0;
  m_OvcTripCnt = eeprom_read_byte((uint8_t*)EOFS_OVC_TRIP_CNT);
  m_GfiSelfTestFailCnt = eeprom_read_byte((uint8_t*)EOFS_GFI_SELF_TEST_FAIL_CNT);
#endif //OVERCURRENT_THRESHOLD

#ifdef TEMPERATURE_MONITORING
  for (uint8_t i=0;i<3;i++) {
    m_OvtTripCnt[i] = eeprom_read_byte((uint8_t*)(EOFS_OVT_TRIP_CNT+i));
  }
#endif // TEMPERATURE_MONITORING

#endif // AMMETER


#ifndef RGBLCD
  m_wFlags |= ECF_MONO_LCD;
#endif

  m_wVFlags = ECVF_DEFAULT;

#ifdef AUTH_LOCK
  if (AutoAuthLockIsEnabled()) AuthLock(1,0);
#endif // AUTH_LOCK

#ifdef GFI
  m_GfiRetryCnt = 0;
  m_GfiTripCnt = eeprom_read_byte((uint8_t*)EOFS_GFI_TRIP_CNT);
#endif // GFI
#ifdef ADVPWR
  m_NoGndRetryCnt = 0;
  m_NoGndTripCnt = eeprom_read_byte((uint8_t*)EOFS_NOGND_TRIP_CNT);

  m_StuckRelayStartTimeMS = 0;
  m_StuckRelayTripCnt = eeprom_read_byte((uint8_t*)EOFS_STUCK_RELAY_TRIP_CNT);

  m_NoGndRetryCnt = 0;
  m_NoGndStart = 0;
#endif // ADVPWR

#ifdef ADVPWR

#ifdef FT_READ_AC_PINS
  while (1) {
    WDT_RESET();
    sprintf(g_sTmp,"%d",(int)ReadACPins());
    g_OBD.LcdMsg("AC Pins",g_sTmp);
  }
#endif // FT_READ_AC_PINS

#ifdef RAPI
  RapiSendEvseState(1); // force send
#endif

#ifdef SHOW_DISABLED_TESTS
  ShowDisabledTests();
#endif

#if defined(POWERSHARE) && defined(LCD16X2)
  if (PowerShareIsEnabled()) {
      g_OBD.LcdMsg_P(g_psPowerShare,g_psEnabled);
      wdt_delay(SHOW_DISABLED_DELAY);
    }
#endif // POWERSHARE
 
    g_OBD.SetRedLed(1); 
#ifdef LCD16X2 //Adafruit RGB LCD
    g_OBD.LcdMsg_P(g_psPwrOn,g_psSelfTest);
#endif //Adafruit RGB LCD 

    uint8_t postcode = doPost(&svclvl); // auto detect service level overrides any saved values
    if (postcode != PS) {
      SetHardFault(); // prevent RAPI from writing LCD
      // UL wants EVSE to hard fault until power cycle if POST fails
#ifdef RAPI
      RapiSendBootNotification(GetState());
      RapiSendEvseState(1);
#endif
      while (1) { // spin forever
        ProcessInputs();
      }
    }
#endif // ADVPWR  

  SetSvcLevel(svclvl);

#ifdef DELAYTIMER
  if (g_DelayTimer.IsTimerEnabled()) {
    Sleep();
  }
#endif

  g_OBD.SetGreenLed(0);

#ifdef RAPI
  RapiSendBootNotification(0);
#endif

#ifdef CALIBRATE
  for (;;) {
    CALIB_DATA cd;
    Calibrate(&cd);
    sprintf(g_sTmp,"P max %d min %d avg %d N max %d min %d avg %d",
	    cd.m_pMax,cd.m_pMin,cd.m_pAvg,cd.m_nMax,cd.m_nMin,cd.m_nAvg);
    Serial.println(g_sTmp);
  }
#endif //CALIBRATE
}

void J1772EVSEController::ReadPilot(uint16_t *plow,uint16_t *phigh)
{
  uint16_t pl = 1023;
  uint16_t ph = 0;

  // 1x = 114us 20x = 2.3ms 100x = 11.3ms
  for (int i=0;i < PILOT_LOOP_CNT;i++) {
    uint16_t reading = adcPilot.read();  // measures pilot voltage
    
    if (reading > ph) {
      ph = reading;
    }
    else if (reading < pl) {
      pl = reading;
    }
  }

  if (m_Pilot.GetState() != PILOT_STATE_N12) {
    // update prev state
    if (EvConnected()) SetEvConnectedPrev();
    else ClrEvConnectedPrev();

    // can determine connected state only if not -12VDC
    if (ph >= m_ThreshData.m_ThreshAB) {
      ClrEvConnected();
#ifdef CHARGE_LIMIT
      ClrChargeLimit();
#endif // CHARGE_LIMIT
#ifdef TIME_LIMIT
      ClrTimeLimit();
#endif // TIME_LIMIT
#if defined(AUTH_LOCK)
      // lock when EV unplugged
      if (AutoAuthLockIsEnabled() && EvConnectedTransition()) AuthLock(1,0);
#endif 
    }
    else {
      SetEvConnected();
    }
  }

  if (plow) {
    *plow = pl;
    *phigh = ph;
  }
}


//TABLE A1 - PILOT LINE VOLTAGE RANGES (recommended.. adjust as necessary
//                           Minimum Nominal Maximum 
//Positive Voltage, State A  11.40 12.00 12.60 
//Positive Voltage, State B  8.36 9.00 9.56 
//Positive Voltage, State C  5.48 6.00 6.49 
//Positive Voltage, State D  2.62 3.00 3.25 
//Negative Voltage - States B, C, D, and F -11.40 -12.00 -12.60 
void J1772EVSEController::Update(uint8_t forcetransition)
{
  uint16_t plow;
  uint16_t phigh = 0xffff;

#ifdef RAPI
  uint8_t force = 0;
#endif

  unsigned long curms = millis();

  if (m_EvseState == EVSE_STATE_DISABLED) {
    m_PrevEvseState = m_EvseState; // cancel state transition
    return;
  }


  ReadPilot(&plow,&phigh); // always read so we can update EV connect state, too

  if (EvConnectedTransition()) {
    if (EvConnected()) {
      m_AccumulatedChargeTime = 0;
      m_ElapsedChargeTime = 0;
    }
  }

  if (m_EvseState == EVSE_STATE_SLEEPING) {
    int8_t cancelTransition = 1;
    if (chargingIsOn()) {
      // wait for pilot voltage to go > STATE C. This will happen if
      // a) EV reacts and goes back to state B (opens its contacts)
      // b) user pulls out the charge connector
      // if it doesn't happen within 3 sec, we'll just open our relay anyway
      // c) no current draw means EV opened its contacts even if it stays in STATE C
      //    allow 3A slop for ammeter inaccuracy
      g_EnergyMeter.readAmmeter();
      if ((phigh >= m_ThreshData.m_ThreshBC)
#ifdef AMMETER
	  || (g_EnergyMeter.getInstantaneousMA() <= 1000L)
#endif // AMMETER
	  || ((curms - m_ChargeOffTimeMS) >= 3000UL)) {
#ifdef FT_SLEEP_DELAY
	//	sprintf(g_sTmp,"SLEEP OPEN %d",(int)phigh);
	//	g_OBD.LcdMsg(g_sTmp,(phigh >= m_ThreshData.m_ThreshBC) ? "THRESH" : "TIMEOUT");
	sprintf(g_sTmp,"%d %lu %lu",phigh,g_EnergyMeter.getInstantaneousMA(),(curms-m_ChargeOffTimeMS));
	g_OBD.LcdMsg(g_sTmp,(phigh >= m_ThreshData.m_ThreshBC) ? "THRESH" : "TIMEOUT");
	chargingOff();
	for(;;)
	wdt_delay(2000);
#endif // FT_SLEEP_DELAY
	chargingOff();
      }
    }
    else { // not charging
#if defined(TIME_LIMIT) || defined(CHARGE_LIMIT)
      if (LimitSleepIsSet()) {
	if (!EvConnected()) {
	  // if we went into sleep due to time/charge limit met, then
	  // automatically cancel the sleep when the car is unplugged
	  cancelTransition = 0;
	  SetLimitSleep(0);
	  m_EvseState = EVSE_STATE_UNKNOWN;
	}
      }
#endif //defined(TIME_LIMIT) || defined(CHARGE_LIMIT)

      if (EvConnectedTransition()) {
#ifdef DELAYTIMER
	if (!EvConnected()) {
	  g_DelayTimer.ClrManualOverride();
        }
#endif // DELAYTIMER
	g_OBD.Update(OBD_UPD_FORCE);
#ifdef RAPI
	RapiSendEvseState();
#endif // RAPI	
      }
    }

    if (cancelTransition) {
      m_PrevEvseState = m_EvseState; // cancel state transition
      return;
    }
  }
#ifdef OVERCURRENT_THRESHOLD
  else if (m_EvseState == EVSE_STATE_OVER_CURRENT) {
    if ((millis()-m_OverCurrentStartMs) >= OVERCURRENT_RETRY_TIMEOUT) {
      m_OverCurrentStartMs = 0;
    }
    else {
      if (!EvConnected()) { // clear it if EV unplugged
	m_EvseState = EVSE_STATE_UNKNOWN;
      }
      else {
	return;
      }
    }
  }
#endif // OVERCURRENT_THRESHOLD



  uint8_t prevevsestate = m_EvseState;
  uint8_t tmpevsestate = EVSE_STATE_UNKNOWN;
  uint8_t nofault = 1;

#ifdef ADVPWR
  uint8_t acpinstate = ReadACPins();
  
  if (chargingIsOn()) { // relay closed
    if ((curms - m_ChargeOnTimeMS) > GROUND_CHK_DELAY) {
      // ground check - can only test when relay closed
      if (GndChkEnabled() && ((acpinstate & 3) == 3)) {
	// bad ground
	tmpevsestate = EVSE_STATE_NO_GROUND;
	m_EvseState = EVSE_STATE_NO_GROUND;
	
	chargingOff(); // open the relay
	if ((prevevsestate != EVSE_STATE_NO_GROUND) && (((uint8_t)(m_NoGndTripCnt+1)) < 254)) {
	  m_NoGndTripCnt++;
	  eeprom_write_byte((uint8_t*)EOFS_NOGND_TRIP_CNT,m_NoGndTripCnt);
	}
	m_NoGndStart = curms;
	
	nofault = 0;
      }

#if defined(AUTOSVCLEVEL)
      if (isOE1()) {
          // if EV was plugged in during POST, we couldn't do AutoSvcLevel detection,
          // so we had to hardcode L1. During first charge session, we can probe and set to L2 if necessary
          if (AutoSvcLvlSkipped() && (m_EvseState == EVSE_STATE_C)) {
            if (!acpinstate) {
              // set to L2
              SetSvcLevel(2,1);
            }
            SetAutoSvcLvlSkipped(0);
          }
        }
#endif // AUTOSVCLEVEL
    }
  }
  else { // !chargingIsOn() - relay open
    if (prevevsestate == EVSE_STATE_NO_GROUND) {
      // check to see if EV disconnected
      if (!EvConnected()) {
	// EV disconnected - cancel fault
	m_EvseState = EVSE_STATE_UNKNOWN;
	return;
      }

      if (((m_NoGndRetryCnt < GFI_RETRY_COUNT) || (GFI_RETRY_COUNT == 255)) &&
	  ((curms - m_NoGndStart) > GFI_TIMEOUT)) {
	m_NoGndRetryCnt++;
      }
      else {
	tmpevsestate = EVSE_STATE_NO_GROUND;
	m_EvseState = EVSE_STATE_NO_GROUND;
	
	nofault = 0;
      }
    }
    else if (StuckRelayChkEnabled()) {    // stuck relay check - can test only when relay open
      if (((acpinstate & 3) != 3)) { // Stuck Relay reading
	if ((prevevsestate != EVSE_STATE_STUCK_RELAY) && !m_StuckRelayStartTimeMS) { //check for first occurence
	  m_StuckRelayStartTimeMS = curms; // mark start state
	}
        if ( ( ((curms - m_ChargeOffTimeMS) > STUCK_RELAY_DELAY) && //  charge off de-bounce
               ((curms - m_StuckRelayStartTimeMS) > STUCK_RELAY_DELAY) ) ||  // start delay de-bounce
	     (prevevsestate == EVSE_STATE_STUCK_RELAY) ) { // already in error state
	  // stuck relay
	  if ((prevevsestate != EVSE_STATE_STUCK_RELAY) && (((uint8_t)(m_StuckRelayTripCnt+1)) < 254)) {
	    m_StuckRelayTripCnt++;
	    eeprom_write_byte((uint8_t*)EOFS_STUCK_RELAY_TRIP_CNT,m_StuckRelayTripCnt);
	  }   
	  tmpevsestate = EVSE_STATE_STUCK_RELAY;
	  m_EvseState = EVSE_STATE_STUCK_RELAY;
	  nofault = 0;
	}
      } // end of stuck relay reading
      else m_StuckRelayStartTimeMS = 0; // not stuck - reset
    } // end of StuckRelayChkEnabled
  } // end of !chargingIsOn() - relay open
#endif // ADVPWR
   
#ifdef GFI
  if (m_Gfi.Fault()) {
    tmpevsestate = EVSE_STATE_GFCI_FAULT;
    m_EvseState = EVSE_STATE_GFCI_FAULT;

    if (prevevsestate != EVSE_STATE_GFCI_FAULT) { // state transition
      if (((uint8_t)(m_GfiTripCnt+1)) < 254) {
	m_GfiTripCnt++;
	eeprom_write_byte((uint8_t*)EOFS_GFI_TRIP_CNT,m_GfiTripCnt);
      }
      m_GfiRetryCnt = 0;
      m_GfiFaultStartMs = curms;
    }
    else { // was already in GFI fault
      // check to see if EV disconnected
      if (!EvConnected()) {
	// EV disconnected - cancel fault
	m_EvseState = EVSE_STATE_UNKNOWN;
	m_Gfi.Reset();
	return;
      }

      if ((curms - m_GfiFaultStartMs) >= GFI_TIMEOUT) {
#ifdef FT_GFI_RETRY
	g_OBD.LcdMsg("Reset","GFI");
	wdt_delay(250);
#endif // FT_GFI_RETRY
	m_GfiRetryCnt++;
	
	if ((GFI_RETRY_COUNT != 255) && (m_GfiRetryCnt > GFI_RETRY_COUNT)) {
	  HardFault();
	  return;
	}
	else {
	  m_Gfi.Reset();
	  m_GfiFaultStartMs = 0;
	}
      }
    }

    nofault = 0;
  }
#endif // GFI


#ifdef TEMPERATURE_MONITORING                 //  A state for OverTemp fault
if (TempChkEnabled()) {
  if (g_TempMonitor.TempExceeded(TEMPERATURE_AMBIENT_PANIC)) {
    if ((prevevsestate != EVSE_STATE_OVER_TEMPERATURE) && (((uint8_t)(m_OvtTripCnt[2]+1)) < 254)) {
      eeprom_write_byte((uint8_t*)(EOFS_OVT_TRIP_CNT+2),++m_OvtTripCnt[2]);
    }

    tmpevsestate = EVSE_STATE_OVER_TEMPERATURE;
    m_EvseState = EVSE_STATE_OVER_TEMPERATURE;
    nofault = 0;
  }
}
#endif // TEMPERATURE_MONITORING

 uint8_t prevpilotstate = m_PilotState;
 uint8_t tmppilotstate = EVSE_STATE_UNKNOWN;

  if (nofault) {
    if ((prevevsestate >= EVSE_FAULT_STATE_BEGIN) &&
	(prevevsestate <= EVSE_FAULT_STATE_END)) {
      // just got out of fault state - pilot back on
      setPilotState(PILOT_STATE_P12);
      prevevsestate = EVSE_STATE_UNKNOWN;
      m_EvseState = EVSE_STATE_UNKNOWN;
    }

    if (DiodeCheckEnabled() && (m_Pilot.GetState() == PILOT_STATE_PWM) && (plow >= m_ThreshData.m_ThreshDS)) {
      // diode check failed
      tmpevsestate = EVSE_STATE_DIODE_CHK_FAILED;
      tmppilotstate = EVSE_STATE_DIODE_CHK_FAILED;
    }
    else if (phigh >= m_ThreshData.m_ThreshAB) {
      // 12V EV not connected
      tmpevsestate = EVSE_STATE_A;
      tmppilotstate = EVSE_STATE_A;
    }
    else if (phigh >= m_ThreshData.m_ThreshBC) {
      // 9V EV connected, waiting for ready to charge
      tmpevsestate = EVSE_STATE_B;
      tmppilotstate = EVSE_STATE_B;
    }
    else if (phigh  >= m_ThreshData.m_ThreshCD) {
      // 6V ready to charge
      tmppilotstate = EVSE_STATE_C;
      if (m_Pilot.GetState() == PILOT_STATE_PWM) {
	tmpevsestate = EVSE_STATE_C;
      }
      else {
	// PWM is off so we can't charge.. force to State B
	tmpevsestate = EVSE_STATE_B;
      }
    }
    else if (phigh > m_ThreshData.m_ThreshD) {
      tmppilotstate = EVSE_STATE_D;
      // 3V ready to charge vent required
      if (VentReqEnabled()) {
	tmpevsestate = EVSE_STATE_D;
      }
      else {
	tmpevsestate = EVSE_STATE_C;
      }
    }
    else {
      tmpevsestate = EVSE_STATE_UNKNOWN;
    }

#ifdef FT_ENDURANCE
    if (nofault) {
      if (((tmpevsestate == EVSE_STATE_A)||(tmpevsestate == EVSE_STATE_B)) && (g_CycleCnt < 0)) {
        g_CycleCnt = 0;
        g_CycleHalfStart = curms;
        g_CycleState = EVSE_STATE_B;
      } 

      if (g_CycleCnt >= 0) {
        if (g_CycleState == EVSE_STATE_B) {
	  if ((curms - g_CycleHalfStart) >= 9000) {
	    g_CycleCnt++;
	    g_CycleHalfStart = curms;
	    tmpevsestate = EVSE_STATE_C;
	    g_CycleState = EVSE_STATE_C;
	  }
	  else tmpevsestate = EVSE_STATE_B;
        }
        else if (g_CycleState == EVSE_STATE_C) {
	  if ((curms - g_CycleHalfStart) >= 1000) {
	    g_CycleHalfStart = curms;
	    tmpevsestate = EVSE_STATE_B;
	    g_CycleState = EVSE_STATE_B;
	  }
	  else tmpevsestate = EVSE_STATE_C;
        }
      }
    }
#endif // FT_ENDURANCE

    // debounce state transitions
    if (tmpevsestate != prevevsestate) {
      if (tmpevsestate != m_TmpEvseState) {
        m_TmpEvseStateStart = curms;
      }
      else if ((curms - m_TmpEvseStateStart) >= ((tmpevsestate == EVSE_STATE_A) ? DELAY_STATE_TRANSITION_A : DELAY_STATE_TRANSITION)) {
        m_EvseState = tmpevsestate;
      }
    }
  } // nofault

  // debounce state transitions
  if (tmppilotstate != prevpilotstate) {
    if (tmppilotstate != m_TmpPilotState) {
      m_TmpPilotStateStart = curms;
    }
    else if ((curms - m_TmpPilotStateStart) >= DELAY_STATE_TRANSITION) {
      m_PilotState = tmppilotstate;
    }
  }


  m_TmpPilotState = tmppilotstate;
  m_TmpEvseState = tmpevsestate;

#ifdef FT_GFI_RETRY
  if (nofault && (prevevsestate == EVSE_STATE_C) && 
      ((curms - m_ChargeOnTimeMS) > 10000)) {
    g_OBD.LcdMsg("Induce","Fault");
    for(int i = 0; i < GFI_TEST_CYCLES; i++) {
      m_Gfi.pinTest.write(1);
      delayMicroseconds(GFI_PULSE_ON_US);
      m_Gfi.pinTest.write(0);
      delayMicroseconds(GFI_PULSE_OFF_US);
      if (m_Gfi.Fault()) break;
    }
  }
#endif // FT_GFI_RETRY

  //
  // check request for state transition to A/B/C
  //

#ifdef AUTH_LOCK
#ifdef AUTH_LOCK_REG
  {
    int8_t locked;
    if (pinAuthLock.read()) locked = 1;
    else locked = 0;

    if (m_EvseState == EVSE_STATE_A) {
      // ignore the pin, and always lock in STATE_A
      locked = 1;
    }

    if (locked != AuthLockIsOn()) {
      AuthLock(locked,0);
      g_OBD.Update(OBD_UPD_FORCE);
      forcetransition = 1;
    }
  }
#endif // AUTH_LOCK_REG

  if (AuthLockIsOn() && (m_EvseState == EVSE_STATE_C)) {
    // force to STATE B when lock is on
    m_EvseState = EVSE_STATE_B;
  }
#endif // AUTH_LOCK

#ifdef BOOTLOCK
  if (AuthLockIsOn() && (m_EvseState == EVSE_STATE_C)) {
    // force to STATE B when lock is on
    m_EvseState = EVSE_STATE_B;
  }
#endif


  if (
#ifdef STATE_TRANSITION_REQ_FUNC
      m_StateTransitionReqFunc &&
#endif // STATE_TRANSITION_REQ_FUNC
      (m_EvseState != prevevsestate) &&
      ((m_EvseState >= EVSE_STATE_A) && (m_EvseState <= EVSE_STATE_C))) {
    m_PilotState = tmppilotstate;
#ifdef STATE_TRANSITION_REQ_FUNC
    uint8_t newstate = (*m_StateTransitionReqFunc)(prevpilotstate,m_PilotState,prevevsestate,m_EvseState);
    if (newstate) {
      m_EvseState = newstate;
    }
#endif // STATE_TRANSITION_REQ_FUNC
  }

  
  // state transition
  if (forcetransition || (m_EvseState != prevevsestate)) {
#ifdef MENNEKES_LOCK
    if (m_EvseState == MENNEKES_LOCK_STATE) {
      m_MennekesLock.Lock();
    }
    else {
      m_MennekesLock.Unlock();
    }
#endif // MENNEKES_LOCK


    if (m_EvseState == EVSE_STATE_A) { // EV not connected
      chargingOff(); // turn off charging current
      setPilotState(PILOT_STATE_P12);
#ifdef DELAYTIMER
	g_DelayTimer.ClrManualOverride();
#endif // DELAYTIMER
#ifdef OVERCURRENT_THRESHOLD
      m_OverCurrentRetryCnt = 0;
#endif // OVERCURRENT_THRESHOLD
#ifdef TEMPERATURE_MONITORING
    g_TempMonitor.ClrOverTemperatureLogged();
#endif
    }
    else if (m_EvseState == EVSE_STATE_B) { // connected 
      chargingOff(); // turn off charging current
#ifdef AUTH_LOCK
      // if locked, don't turn on PWM
      if (AuthLockIsOn()) {
	setPilotState(PILOT_STATE_P12);
      }
      else {
	setPilotPWM(m_CurrentCapacity);
      }
#else
      setPilotPWM(m_CurrentCapacity);
#endif // AUTH_LOCK
    }
    else if (m_EvseState == EVSE_STATE_C) {
      DoStateCTransition();
    }
    else if (m_EvseState == EVSE_STATE_D) {
      // vent required not supported
      chargingOff(); // turn off charging current
      setPilotState(PILOT_STATE_P12);
      HardFault();
    }
    else if (m_EvseState == EVSE_STATE_GFCI_FAULT) {
      // vehicle state F
      chargingOff(); // turn off charging current
      setPilotState(PILOT_STATE_P12);
    }
#ifdef TEMPERATURE_MONITORING
    else if (m_EvseState == EVSE_STATE_OVER_TEMPERATURE) {
      // vehicle state Over Teperature within the EVSE
      setPilotState(PILOT_STATE_P12); // Signal the EV to pause, high current should cease within five seconds
      
      while ((millis()-curms) < 5000) {
	WDT_RESET();
      }
      chargingOff(); // open the EVSE relays hopefully the EV has already disconnected by now by the J1772 specification
      setPilotState(PILOT_STATE_P12);
      HardFault();
    }
#endif //TEMPERATURE_MONITORING
    else if (m_EvseState == EVSE_STATE_DIODE_CHK_FAILED) {
      chargingOff(); // turn off charging current
      // must leave pilot on so we can keep checking
      // N.B. J1772 specifies to go to State F (-12V) but we can't do that
      // and keep checking
      setPilotPWM(m_CurrentCapacity);
      setPilotState(PILOT_STATE_P12);
      HardFault();
    }
    else if (m_EvseState == EVSE_STATE_NO_GROUND) {
      // Ground not detected
      chargingOff(); // turn off charging current
      setPilotState(PILOT_STATE_P12);
    }
    else if (m_EvseState == EVSE_STATE_STUCK_RELAY) {
      // Stuck relay detected
      chargingOff(); // turn off charging current
      // set N12 so it doesn't auto cancel if no EV plugged in, and unplug won't cancel it
      setPilotState(PILOT_STATE_N12);
#ifdef UL_COMPLIANT
      // per discussion w/ UL Fred Reyes 20150217
      // always hard fault stuck relay
      HardFault();
#endif // UL_COMPLIANT
    }
    else {
      setPilotState(PILOT_STATE_P12);
      chargingOff(); // turn off charging current
    }

  } // state transition

#ifdef UL_COMPLIANT
  if (!nofault && (prevevsestate == EVSE_STATE_C)) {
    // if fault happens immediately (within 2 sec) after charging starts, hard fault
    if ((curms - m_ChargeOnTimeMS) <= 2000) {
      HardFault();
      return;
    }
  }
#endif // UL_COMPLIANT

  m_PrevEvseState = prevevsestate;

#ifdef AMMETER
  if (m_EvseState == EVSE_STATE_C) {
    int32_t ima = g_EnergyMeter.getInstantaneousMA();
    int32_t occ = m_ChargingCurrent;
    m_ChargingCurrent = g_EnergyMeter.getAvgMA();
    int32_t delta = ima-m_LastCCDirMa;
    if (delta > 1000L) {
      m_ChargingCurrentDir = 1;
      m_LastCCDirMa = ima;
      force = 1;
    }
    else if (delta < -1000L) {
      m_ChargingCurrentDir = -1;
      m_LastCCDirMa = ima;
      force = 1;
    }
    else {
      m_ChargingCurrentDir = 0;
    }
    if (occ != m_ChargingCurrent) g_OBD.SetAmmeterDirty(1);
  }

#ifdef OVERCURRENT_THRESHOLD
  if (m_EvseState == EVSE_STATE_C) {
    if (GetChargingCurrent() >= ((m_CurrentCapacity+OVERCURRENT_THRESHOLD)*1000L)) {
      if (m_OverCurrentStartMs) { // already in overcurrent state
	if ((millis()-m_OverCurrentStartMs) >= OVERCURRENT_TIMEOUT) {
	  if (((uint8_t)(m_OvcTripCnt+1)) < 254) {
	    m_OvcTripCnt++;
	    eeprom_write_byte((uint8_t*)EOFS_OVC_TRIP_CNT,m_OvcTripCnt);
	  }

	  //
	  // overcurrent for too long. stop charging and fault
	  //
	  m_EvseState = EVSE_STATE_OVER_CURRENT;

	  setPilotState(PILOT_STATE_P12); // Signal the EV to pause
	  curms = millis();
	  while ((millis()-curms) < 1000) { // give EV 1s to stop charging
	    g_EnergyMeter.readAmmeter();
	    if (g_EnergyMeter.getInstantaneousMA() <= 1000L) break;
	  }
	  m_OverCurrentAmps = (uint8_t)(GetChargingCurrent()/1000-m_CurrentCapacity);
	  chargingOff(); // open the EVSE relays hopefully the EV has already discon

	  m_OverCurrentStartMs = millis(); // save start of this overcurrent event
	  if (++m_OverCurrentRetryCnt > OVERCURRENT_MAX_RETRIES) {
	    // spin until EV is disconnected
	    HardFault();
	  }
	}
      }
      else {
	m_OverCurrentStartMs = millis();
      }
    }
    else {
      m_OverCurrentStartMs = 0; // clear overcurrent
    }
  }
  else {
    m_OverCurrentStartMs = 0; // clear overcurrent
  }
#endif // OVERCURRENT_THRESHOLD    
#endif // AMMETER

#ifdef TEMPERATURE_MONITORING
  if(TempChkEnabled()) {
      uint8_t currcap = GetMaxCurrentCapacity();
      uint8_t setit = 0;

      if (!g_TempMonitor.OverTemperature() && g_TempMonitor.TempExceeded(TEMPERATURE_AMBIENT_THROTTLE_DOWN)) {   // Throttle back the L2 current advice to the EV
	currcap /= 2;   // set to the throttled back level
	setit = 2;
        if (((uint8_t)(m_OvtTripCnt[0]+1)) < 254) {
          eeprom_write_byte((uint8_t*)EOFS_OVT_TRIP_CNT,++m_OvtTripCnt[0]);
        }
      }

      else if (g_TempMonitor.OverTemperature() && !g_TempMonitor.TempExceeded(TEMPERATURE_AMBIENT_RESTORE_AMPERAGE)) {  // restore the original L2 current advice to the EV
	setit = 1;    // set to the user's original setting for current
      }           
      
      
      else if (!g_TempMonitor.OverTemperatureShutdown() && g_TempMonitor.TempExceeded(TEMPERATURE_AMBIENT_SHUTDOWN)) {   // Throttle back the L2 current advice to the EV
  currcap /= 4;
	setit = 4;
        if (((uint8_t)(m_OvtTripCnt[1]+1)) < 254) {
          eeprom_write_byte((uint8_t*)(EOFS_OVT_TRIP_CNT+1),++m_OvtTripCnt[1]);
        }
      }
      
      else if (g_TempMonitor.OverTemperatureShutdown() && !g_TempMonitor.TempExceeded(TEMPERATURE_AMBIENT_THROTTLE_DOWN)) {  //  restore the throttled down current advice to the EV since things have cooled down again
	currcap /= 2;    // set to the throttled back level
	setit = 3;
      }    
      if (setit) {
        if (setit <= 2) {
          g_TempMonitor.SetOverTemperature(setit-1);
	}
        else {
	  g_TempMonitor.SetOverTemperatureShutdown(setit-3);
	}
	SetCurrentCapacity(currcap,0,1);
    	if (m_Pilot.GetState() != PILOT_STATE_PWM) {
    	  setPilotPWM(m_CurrentCapacity);
        }
      }
  }
#endif // TEMPERATURE_MONITORING

  if (m_EvseState == EVSE_STATE_C) {
    m_ElapsedChargeTimePrev = m_ElapsedChargeTime;
    m_ElapsedChargeTime = now() - m_ChargeOnTime;

#ifdef CHARGE_LIMIT
    if (m_chargeLimitTotWs && (g_EnergyMeter.GetSessionWs() >= m_chargeLimitTotWs)) {
      ClrChargeLimit(); // clear charge limit
#ifdef TIME_LIMIT
      ClrTimeLimit(); // clear time limit
#endif // TIME_LIMIT
      SetLimitSleep(1);
      Sleep();
    }
#endif
#ifdef TIME_LIMIT
    if (m_timeLimitEnd) {
      // must call millis() below because curms is sampled before transition to
      // to State C, so m_ChargeOnTimeMS will be > curms from the start
      if (GetElapsedChargeTime() >= m_timeLimitEnd) {
	ClrTimeLimit(); // clear time limit
#ifdef CHARGE_LIMIT
	ClrChargeLimit(); // clear charge limit
#endif // CHARGE_LIMIT
	SetLimitSleep(1);
	Sleep();
      }
    }
#endif // TIME_LIMIT
  }

#ifdef RAPI
    RapiSendEvseState(force);
#endif // RAPI

#ifdef AUTH_LOCK
  if ((m_EvseState != prevevsestate) ||
      (m_PilotState != prevpilotstate)) {
    g_OBD.Update(OBD_UPD_FORCE);
  }
#endif // AUTH_LOCK

  return;
}

#ifdef CALIBRATE
// read ADC values and get min/max/avg for pilot steady high/low states
void J1772EVSEController::Calibrate(PCALIB_DATA pcd)
{
  uint16_t pmax,pmin,pavg,nmax,nmin,navg;

  for (int l=0;l < 2;l++) {
    int reading;
    uint32_t tot = 0;
    uint16_t plow = 1023;
    uint16_t phigh = 0;
    uint16_t avg = 0;
    setPilotState(l ? PILOT_STATE_N12 : PILOT_STATE_P12);

    wdt_delay(250); // wait for stabilization

    // 1x = 114us 20x = 2.3ms 100x = 11.3ms
    int i;
    for (i=0;i < 1000;i++) {
      reading = adcPilot.read();  // measures pilot voltage

      if (reading > phigh) {
        phigh = reading;
      }
      else if (reading < plow) {
        plow = reading;
      }

      tot += reading;
    }
    avg = tot / i;

    if (l) {
      nmax = phigh;
      nmin = plow;
      navg = avg;
    }
    else {
      pmax = phigh;
      pmin = plow;
      pavg = avg;
    }
  }
  pcd->m_pMax = pmax;
  pcd->m_pAvg = pavg;
  pcd->m_pMin = pmin;
  pcd->m_nMax = nmax;
  pcd->m_nAvg = navg;
  pcd->m_nMin = nmin;
}
#endif // CALIBRATE

int J1772EVSEController::SetCurrentCapacity(uint8_t amps,uint8_t updatelcd,uint8_t nosave)
{
  int rc = 0;
  uint8_t maxcurrentcap = (GetCurSvcLevel() == 1) ? MAX_CURRENT_CAPACITY_L1 : MAX_CURRENT_CAPACITY_L2;

  if (nosave) {
    // temporary amps can't be > max set in EEPROM
    maxcurrentcap = GetMaxCurrentCapacity();
  }

#ifdef PP_AUTO_AMPACITY
  if ((GetState() >= EVSE_STATE_B) && (GetState() <= EVSE_STATE_C)) {
    uint8_t mcc = g_ACCController.ReadPPMaxAmps();
    if (mcc && (mcc < maxcurrentcap)) {
      maxcurrentcap = mcc;
    }
  }
#endif // PP_AUTO_AMPACITY

  if ((amps >= MIN_CURRENT_CAPACITY_J1772) && (amps <= maxcurrentcap)) {
    m_CurrentCapacity = amps;
  }
  else if (amps < MIN_CURRENT_CAPACITY_J1772) {
    m_CurrentCapacity = MIN_CURRENT_CAPACITY_J1772;
    rc = 1;
  }
  else {
    m_CurrentCapacity = maxcurrentcap;
    rc = 2;
  }

  if (!nosave) {
    eeprom_write_byte((uint8_t*)((GetCurSvcLevel() == 1) ? EOFS_CURRENT_CAPACITY_L1 : EOFS_CURRENT_CAPACITY_L2),(byte)m_CurrentCapacity);
  }

  if (m_Pilot.GetState() == PILOT_STATE_PWM) {
    setPilotPWM(m_CurrentCapacity);
  }

  if (updatelcd) {
    g_OBD.Update(OBD_UPD_FORCE);
  }

  return rc;
}

#if defined(GFI) || defined(ADVPWR)
unsigned long J1772EVSEController::GetResetMs()
{
#ifdef GFI
  return GFI_TIMEOUT - (millis() - ((m_EvseState == EVSE_STATE_GFCI_FAULT) ? m_GfiFaultStartMs : m_NoGndStart));
#else
  return GFI_TIMEOUT - (millis() - m_NoGndStart);
#endif // GFI
}
#endif // GFI || ADVPWR


#ifdef OVERCURRENT_THRESHOLD
unsigned long J1772EVSEController::GetOverCurrentRetryMs()
{
  return OVERCURRENT_RETRY_TIMEOUT - (millis() - m_OverCurrentStartMs);
}
#endif // OVERCURRENT_THRESHOLD

#ifdef CHARGE_LIMIT
void J1772EVSEController::SetChargeLimitkWh(uint8_t kwh)
{
  if (kwh) {
    m_chargeLimitkWh = kwh;
    // extend session by kwh
    //    m_chargeLimitTotWs = g_EnergyMeter.GetSessionWs() + (3600000ul * (uint32_t)kwh);
    // set session kwh limit
    m_chargeLimitTotWs = (3600000ul * (uint32_t)kwh);
#ifdef DELAYTIMER
    g_DelayTimer.SetManualOverride();
#endif // DELAYTIMER
    setVFlags(ECVF_CHARGE_LIMIT);
  }
  else {
    ClrChargeLimit();
#ifdef DELAYTIMER
    g_DelayTimer.ClrManualOverride();
#endif // DELAYTIMER
  }
}
#endif // CHARGE_LIMIT

#ifdef TIME_LIMIT
void J1772EVSEController::SetTimeLimit15(uint8_t mind15)
{
  if (mind15) {
    m_timeLimit15 = mind15;
    // extend session by mind15 15 min increments
    //    m_timeLimitEnd = GetElapsedChargeTime() + (time_t)(15lu*60lu * (unsigned long)mind15);
    // set session time limit in 15 min increments
    m_timeLimitEnd = (time_t)(15lu*60lu * (unsigned long)mind15);
#ifdef DELAYTIMER
    g_DelayTimer.SetManualOverride();
#endif // DELAYTIMER
    setVFlags(ECVF_TIME_LIMIT);
  }
  else {
    ClrTimeLimit();
#ifdef DELAYTIMER
    g_DelayTimer.ClrManualOverride();
#endif // DELAYTIMER
  }

}
#endif // TIME_LIMIT


void J1772EVSEController::setPilotState(PILOT_STATE pstate)
{
  //  clrVFlags(ECVF_PWM_ON);
  m_Pilot.SetState(pstate);
}

int J1772EVSEController::setPilotPWM(int amps)
{
  //  setVFlags(ECVF_PWM_ON);
  return m_Pilot.SetPWM(amps);
} // 12V 1KHz PWM


void J1772EVSEController::DoStateCTransition()
{
  m_EvseState = EVSE_STATE_C;

  setPilotPWM(m_CurrentCapacity);
#if defined(UL_GFI_SELFTEST) && !defined(NOCHECKS)
  // test GFI before closing relay
  if (GfiSelfTestEnabled() && m_Gfi.SelfTest()) {
    // GFI test failed - hard fault
    _logGfiSelfTestFail();
    m_EvseState = EVSE_STATE_GFI_TEST_FAILED;
    setPilotState(PILOT_STATE_P12);
    HardFault();
    return;
  }
#endif // UL_GFI_SELFTEST
  
#ifdef FT_GFI_LOCKOUT
  for(int i = 0; i < GFI_TEST_CYCLES; i++) {
    m_Gfi.pinTest.write(1);
    delayMicroseconds(GFI_PULSE_ON_US);
    m_Gfi.pinTest.write(0);
    delayMicroseconds(GFI_PULSE_OFF_US);
    if (m_Gfi.Fault()) break;
  }
  g_OBD.LcdMsg("Closing","Relay");
  wdt_delay(150);
#endif // FT_GFI_LOCKOUT
  
  chargingOn(); // turn on charging current
}

#ifdef POWERSHARE
void J1772EVSEController::PowerShareEnable(uint8_t tf)
{
  if (tf) {
    setFlags(ECF_PWRSHARE_ENABLED);
  }
  else {
    clrFlags(ECF_PWRSHARE_ENABLED);
  }
  SaveEvseFlags();
  //  Reboot();
}

#endif // POWERSHARE

#ifdef CALIBRATION
uint32_t J1772EVSEController::GetCalibRawMean()
{
  MovingAverage ma;
  uint32_t avg;
  for (int i=0;i < (MA_PTS+2);i++) {
    UNION4B u;
    u.u32 = readAmmeter();
    avg = ma.Calc(u.u32);
  }
  return avg;
}
#endif // CALIBRATION

//-- end J1772EVSEController
