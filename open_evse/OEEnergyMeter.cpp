#include "open_evse.h"
#include "OEEnergyMeter.h"

#ifdef OE_AMMETER
static inline unsigned long ulong_sqrt(unsigned long in)
{
  unsigned long out = 0;
  unsigned long bit = 0x40000000ul;

  // "bit" starts at the highest power of four <= the argument.
  while (bit > in)
    bit >>= 2;

  while (bit) {
    unsigned long sum = out + bit;
    if (in >= sum) {
      in -= sum;
      out = (out >> 1) + bit;
    }
    else
      out >>= 1;
    bit >>= 2;
  }

  return out;
}

//#define MA_PTS 32 // # points in moving average MUST BE power of 2
//#define MA_BITS 5 // log2(MA_PTS)
#define MA_PTS 16 // # points in moving average MUST BE power of 2
#define MA_BITS 4 // log2(MA_PTS)
//#define MA_PTS 8 // # points in moving average MUST BE power of 2
//#define MA_BITS 3 // log2(MA_PTS)

// to save memory
// instead of doing a real moving average we just do a non-overlapping
// sliding window and output a value every MA_PTS
uint32_t MovingAverage::Calc(uint32_t samp)
{
  if (curidx == 0) {
    tot = 0;
    hi = 0;
    lo = 4294967295;
  }

  tot += samp;
  if (samp > hi) hi = samp;
  if (samp < lo) lo = samp;

  if (++curidx == (MA_PTS+2)) {
    curidx = 0;
    tot -= lo;
    tot -= hi;
    return tot >> MA_BITS; // tot / MA_PTS
  }
  return 0xffffffff;
}
#endif // OE_AMMETER

OEEnergyMeter::OEEnergyMeter() :
#ifdef OE2_VOLTMETER_PIN
  _adcVoltMeter(OE2_VOLTMETER_PIN)
#endif
#if defined(OE_AMMETER)
  ,_adcCurrent(CURRENT_PIN)
#endif

{}

#ifdef OE_AMMETER
uint32_t OEEnergyMeter::_readAmmeterSample()
{
  WDT_RESET();
  unsigned long sum = 0;
  unsigned int zero_crossings = 0;
  unsigned long last_zero_crossing_time = 0, now_ms;
  long last_sample = -1; // should be impossible - the A/d is 0 to 1023.
  unsigned int sample_count = 0;
  for(unsigned long start = millis(); ((now_ms = millis()) - start) < CURRENT_SAMPLE_INTERVAL; ) {
    long sample = (long) _adcCurrent.read();
    // If this isn't the first sample, and if the sign of the value differs from the
    // sign of the previous value, then count that as a zero crossing.
    if (last_sample != -1 && ((last_sample > 512) != (sample > 512))) {
      // Once we've seen a zero crossing, don't look for one for a little bit.
      // It's possible that a little noise near zero could cause a two-sample
      // inversion.
      if ((now_ms - last_zero_crossing_time) > CURRENT_ZERO_DEBOUNCE_INTERVAL) {
        zero_crossings++;
        last_zero_crossing_time = now_ms;
      }
    }
    last_sample = sample;
    switch(zero_crossings) {
    case 0: 
      continue; // Still waiting to start sampling
    case 1:
    case 2:
      // Gather the sum-of-the-squares and count how many samples we've collected.
      sum += (unsigned long)((sample - 512) * (sample - 512));
      sample_count++;
      continue;
    case 3:
      {
        // The answer is the square root of the mean of the squares.
        // But additionally, that value must be scaled to a real current value.
        // we will do that elsewhere
        uint32_t reading = ulong_sqrt(sum / sample_count);
        _instantaneousMA = (int32_t) (reading * _currentScaleFactor - _currentOffset);
        if (_instantaneousMA < 0) _instantaneousMA = 0;
        return reading;
      }
    }
  }
  WDT_RESET();
  
  // ran out of time. Assume that it's simply not oscillating any. 
  _instantaneousMA = 0;
  return 0;
}

int8_t OEEnergyMeter::readAmmeter()
{
  int32_t mavg = _mavgA.Calc(_readAmmeterSample());
  if (mavg == (int32_t)0xffffffff) {
    return 1;
  }
  else {
    _avgMA = mavg * _currentScaleFactor - _currentOffset;
    if (_avgMA < 0) {
      _avgMA = 0;
    }
    return 0;
  }
}

void OEEnergyMeter::setAmmeterCalibration(int16_t scalefactor, int16_t offset,bool writeeep)
{
  _currentScaleFactor = scalefactor;
  _currentOffset = offset;
  if (writeeep) {
    eeprom_write_word((uint16_t*)EOFS_CURRENT_SCALE_FACTOR,scalefactor);
    eeprom_write_word((uint16_t*)EOFS_AMMETER_CURR_OFFSET,offset);
  }
}

#endif // OE_AMMETER

#ifdef OE2_VOLTMETER_PIN
uint32_t OEEnergyMeter::readOe2Voltmeter(uint16_t *adchigh,uint16_t *adclow)
{
  uint16_t hi = 0;
  uint16_t lo = 1023;
  for(uint32_t start_time = millis(); (millis() - start_time) < VOLTMETER_POLL_INTERVAL; ) {
    uint16_t  val = _adcVoltMeter.read();
    if (val > hi) hi = val;
    if (val < lo) lo = val;
  }
  if (adchigh) *adchigh = (uint16_t)hi;
  if (adclow) *adclow = (uint16_t)lo;
  return hi;
}
#endif // OE2_VOLTMETER_PIN


