/*
 * Copyright (c) 2019-2022 Sam C. Lin
 */
// -*- C++ -*-
#pragma once

class MovingAverage {
  uint32_t tot;
  uint32_t hi;
  uint32_t lo;
  int8_t curidx;
public:
 MovingAverage() : tot(0),hi(0),lo(4294967295),curidx(0) {}
  uint32_t Calc(uint32_t samp);
};


// OpenEVSE onboard ammeter
class OEEnergyMeter {
#ifdef OE2_VOLTMETER_PIN
  AdcPin _adcVoltMeter;
#endif
#ifdef OE_AMMETER
  MovingAverage _mavgA;
  int16_t _currentScaleFactor;
  int16_t _currentOffset;
  int32_t _instantaneousMA = 0;
  int32_t _avgMA;
#ifdef CURRENT_PIN
  AdcPin _adcCurrent;
#endif
  uint32_t _readAmmeterSample();
#endif // OE_AMMETER


 public:
  OEEnergyMeter();

#ifdef OE_AMMETER
  int8_t readAmmeter();
  void setAmmeterCalibration(int16_t scalefactor, int16_t offset,bool writeeep=false);
  int16_t getAmmeterOffset() { return _currentOffset; }
  int16_t getAmmeterScaleFactor() { return _currentScaleFactor; }
  int32_t getAvgMA() { return _avgMA; }
  int32_t getInstantaneousMA() { return _instantaneousMA; }
#endif // OE_AMMETER
  
#ifdef OE2_VOLTMETER_PIN
  uint32_t readOe2Voltmeter(uint16_t *adchigh=NULL,uint16_t *adclow=NULL);
#endif
};
