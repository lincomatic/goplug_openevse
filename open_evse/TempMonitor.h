// -*- C++ -*-
#ifdef TEMPERATURE_MONITORING
#include "./TMP75.h"
#include "./MCP9808.h"  //  adding the ambient temp sensor to I2C

#define TEMPERATURE_NOT_INSTALLED -2560 // fake temp to return when hardware not installed
#define TEMPMONITOR_UPDATE_INTERVAL 1000ul
// TempMonitor.m_Flags
#define TMF_OVERTEMPERATURE          0x01
#define TMF_OVERTEMPERATURE_SHUTDOWN 0x02
#define TMF_BLINK_ALARM              0x04
#define TMF_OVERTEMPERATURE_LOGGED   0x08
#define TMF_DS3231_NOT_INSTALLED     0x80
class TempMonitor {
  uint8_t m_Flags;
  unsigned long m_LastUpdate;
  void readDs3231();
  void readMcp9808();
  void readTmp75();
#ifdef MCP9808_IS_ON_I2C
  // these three temperatures need to be signed integers
  MCP9808 m_mcp9808;
  int16_t m_MCP9808_tempx10;  // 230 means 23.0C  Using an integer to save on floating point library use
  int16_t m_MCP9808_tempx16;  // unscaled value from MCP9808
#endif  //MCP9808_IS_ON_I2C
#ifdef USE_DS3231
  int16_t m_DS3231_tempx10;   // the DS3231 RTC has a built in temperature sensor
  int16_t m_DS3231_tempx16;
#endif // USE_DS3231
#ifdef USE_TMP75
  TMP75 m_tmp75;
  int16_t m_TMP75_tempx10;
  int16_t m_TMP75_tempx16;
#endif // USE_TMP75
public:

  TempMonitor() {}
  void Init();
  void Read();

  int16_t GetTempC10(); // get highest priority temp
  int16_t GetTempC16(); // get highest priority temp


  int8_t Mcp9808IsPresent() {
#ifdef MCP9808_IS_ON_I2C
    return m_mcp9808.isPresent();
#else
    return 0;
#endif
  }

  int8_t Tmp75IsPresent() {
#ifdef USE_TMP75
    return m_tmp75.isPresent();
#else
    return 0;
#endif // USE_TMP75
  }

  int8_t Ds3231IsPresent() {
#ifdef USE_DS3231
    return (m_Flags & TMF_DS3231_NOT_INSTALLED) ? 0 : 1;
#else
    return 0;
#endif // USE_DS3231
  }

  int16_t Mcp9808GetTempC10() {
#ifdef MCP9808_IS_ON_I2C
    return m_MCP9808_tempx10;
#else
    return TEMPERATURE_NOT_INSTALLED;
#endif
  }

  int16_t Tmp75GetTempC10() {
#ifdef USE_TMP75
    return m_TMP75_tempx10;
#else
    return TEMPERATURE_NOT_INSTALLED;
#endif
  }

  int16_t Ds3231GetTempC10() {
#ifdef USE_DS3231
    return m_DS3231_tempx10;
#else
    return TEMPERATURE_NOT_INSTALLED;
#endif
  }

  void SetBlinkAlarm(int8_t tf) {
    if (tf) m_Flags |= TMF_BLINK_ALARM;
    else m_Flags &= ~TMF_BLINK_ALARM;
  }
  int8_t BlinkAlarm() { return (m_Flags & TMF_BLINK_ALARM) ? 1 : 0; }
  void SetOverTemperature(int8_t tf) {
    if (tf) m_Flags |= (TMF_OVERTEMPERATURE|TMF_OVERTEMPERATURE_LOGGED);
    else m_Flags &= ~TMF_OVERTEMPERATURE;
  }
  int8_t OverTemperature() { return (m_Flags & TMF_OVERTEMPERATURE) ? 1 : 0; }
  void SetOverTemperatureShutdown(int8_t tf) {
    if (tf) m_Flags |= (TMF_OVERTEMPERATURE_SHUTDOWN|TMF_OVERTEMPERATURE_LOGGED);
    else m_Flags &= ~TMF_OVERTEMPERATURE_SHUTDOWN;
  }
  int8_t OverTemperatureShutdown() { return (m_Flags & TMF_OVERTEMPERATURE_SHUTDOWN) ? 1 : 0; }
  uint8_t OverTemperatureLogged() { return (m_Flags & TMF_OVERTEMPERATURE_LOGGED) ? 1 : 0; }
  void ClrOverTemperatureLogged() { m_Flags &= ~TMF_OVERTEMPERATURE_LOGGED; }

  int8_t TempExceeded(int16_t temperature);
};

extern TempMonitor g_TempMonitor;

#endif // TEMPERATURE_MONITORING
