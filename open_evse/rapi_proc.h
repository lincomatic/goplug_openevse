// -*- C++ -*-
/*
 * Open EVSE Firmware
 *
 * Copyright (c) 2013-2021 Sam C. Lin <lincomatic@gmail.com>
 *
 * This file is part of Open EVSE.

 * Open EVSE is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.

 * Open EVSE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Open EVSE; see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.


 **** RAPI protocol ****

Fx - function
Sx - set parameter
Gx - get parameter

command formats
1. with XOR checksum (recommended)
$cc pp pp ...^xk\r
2. with additive checksum (legacy)
$cc pp pp ...*ck\r
3. no checksum (FOR TESTING ONLY! DON'T USE FOR APPS)
$cc pp pp ...\r
4. checksum + sequence id (v3.0.0+)
$cc pp pp .. :ss^xk\r

\r = carriage return = 13d = 0x0D
cc = 2-letter command
pp = parameters
xk = 2-hex-digit checksum - 8-bit XOR of all characters before '^'
ck = 2-hex-digit checksum - 8-bit sum of all characters before '*'
ss = optional 2-hex-digit sequence id - response will echo the sequence id
     so that receiver can verify that the response matches the command
     ss CANNOT be 00, which is reserved as an invalid value


response format (v1.0.3-)
$OK [optional parameters]\r - success
$NK [optional parameters]\r - failure

response format (vG7.0.0+)
$OK `cc [optional parameters]^xk\r - success
$NK `cc [optional parameters]^xk\r - failure
xk = 2-hex-digit checksum - 8-bit XOR of all characters before '^'

response format (v3.0.0+)
$OK [optional parameters] [:ss]^xk\r - success
$NK [optional parameters] [:ss]^xk\r - failure
xk = 2-hex-digit checksum - 8-bit XOR of all characters before '^'
ss = optional 2-hex-digit sequence ID which was sent with the command
     only present if a sequence ID was send with the command

response format (v3.0.0+)
$OK [optional parameters] [:ss]^xk\r - success
$NK [optional parameters] [:ss]^xk\r - failure
xk = 2-hex-digit checksum - 8-bit XOR of all characters before '^'
ss = optional 2-hex-digit sequence ID which was sent with the command
     only present if a sequence ID was send with the command

A-prefix: asynchronous notification messages

Boot Notification
$AB postcode fwrev
 postcode(hex):
   preboot postcode = FF
   if after POST if boot OK then postcode = 00
   if error then postcode
       07 = open ground
       08 = relay stuck closed or test pin stuck HIGH
       09 = GFI test failed
       0c = pilot error
 fwrev(string): firmware revision
2 $ABs are sent out at boot
$AB FF <fw rev> - at init
$AB <postcode> <fw rev> - after POST

EVSE state transition: sent whenever EVSE state changes
$AT evsestate pilotstate currentcapacity vflags chrgcurrent chrgcurrentdir
 evsestate(hex): EVSE_STATE_xxx
 pilotstate(hex): EVSE_STATE_xxx
 currentcapacity(decimal): amps - active pilot current capacity
 vflags(hex): m_wVFlags bits
 chrgcurrent(dec): instantaneous charging current (not averaged), mA
 chrgcurrentdir(dec): -1=current is dropping, 0=unchanged, 1=rising
 N.B. chrgcurrent,chrgcurrentdir only valid in STATE C (3). currentdir
 is set when current changes by >= 1A within 30 sec

$AW req - WiFi client commands
 req(dec): request
           0 = reset WiFi network (e.g. go to AP mode)
           1 = factory reset

$AN btnpress - button notification
 btnpress: button press type
           1 = short press
           2 = long press
v2.0.1+: 2-hex-digit XOR checksum appended to asynchronous messages

commands


F0 {0|1|2}- enable/disable LCD display updates
 $F0 0^42 - EVSE will not write to LCD except $FP
 $F0 1^43 - EVSE updates LCD, accepts $FP but may overwrite msg
 $F0 2    - EVSE will NEVER write LCD except $FP
F1 - simulate front panel button short press
 N.B.: it is possible that an asynchronous state change will be sent by the
  EVSE prior to sending the response to $F1
FB color - set LCD backlight color
colors:
 OFF 0
 RED 1
 YELLOW 3
 GREEN 2
 TEAL 6
 BLUE 4
 VIOLET 5
 WHITE 7 

 $FB 7*03 - set backlight to white
FC [r]- Clear EEPROM (factory reset) and reboot
 if r not specified, clear only lower 512 bytes
FC c ofs - clear EEPROM byte at offset ofs (write 0xFF into it)
FD - disable EVSE
 $FD*AE
FE - enable EVSE
 $FE*AF
FP x y text - print text on lcd display
 text: replace spaces in the string with 0x7f (DEL)
 returns $NK and fails when in hard fault or POST error
FR - restart EVSE
 $FR*BC
FS - sleep EVSE
 $FS*BD
FF - enable/disable feature
 $FF feature_id 0|1
 0|1 0=disable 1=enable
 feature_id:
  A = auto auth lock
    auto auth lock disabled: auth lock manually control by $S4 only
    auto auth lock enabled: charging not allowed unless $S4 0 (auth lock off) received. EVSE then allows charging until EV unplugged, and then automatically re-locks
  B = disable/enable front panel button
  D = Diode check
  F = GFI self test
  G = Ground check
  K = eGolf/Volvo kludge - must #define EGOLF_KLUDGE
  R = stuck Relay check
  T = temperature monitoring
  V = Vent required check
 $FF D 0 - disable diode check
 $FF G 1 - enable ground check

S0 0|1 - set LCD type
 $S0 0*F7 = monochrome backlight
 $S0 1*F8 = RGB backlight
S1 yr mo day hr min sec - set clock (RTC) yr=2-digit year
S3 cnt - set charge time limit to cnt*15 minutes (0=disable, max=255)
 NOTES:
  - allowed only when EV connected in State B or C
  - current session will stop when time reached. the limit automatically
    gets cancelled when EV disconnected
  - temporarily disables delay timer until EV disconnected or limit reached
 response:
  $OK - accepted
  $NK - invalid EVSE state
S4 0|1 - set auth lock (needs AUTH_LOCK defined and AUTH_LOCK_REG undefined)
   0 = unlocked
   1 = locked
   when auto auth lock is enabled (see $FF A) and on, EVSE will not transition to State C and a lock icon is displayed in States A & B.
   when auto auth lock is enabled, automatically re-locks after EV unplugged

SA currentscalefactor currentoffset - set ammeter settings
SB - clear boot lock
  when BOOTLOCK is defined, EVSE won't allow charging after boot up until SB is sent
 response: $OK 0 = unlock success
           $OK 1 = unlock fail - EVSE currently in fault state

SC amps [V]- set current capacity
 response:
   if amps < minimum current capacity, will set to minimum and return $NK ampsset
   if amps > maximum current capacity, will set to maximum and return $NK ampsset
   if in over temperature status, raising current capacity will fail and return $NK ampsset
   otherwise return $OK ampsset
   ampsset: the resultant current capacity
   default action is to save new current capacity to EEPROM.
   if V is specified, then new current capacity is volatile, and will be
     reset to previous value at next reboot
SH kWh - set cHarge limit to kWh
 NOTES:
  - allowed only when EV connected in State B or C
  - current session will stop when total kWh reached. the limit automatically
    gets cancelled when EV disconnected
  - temporarily disables delay timer until EV disconnected or limit reached
 response:
  $OK - accepted
  $NK - invalid EVSE state
SK - set accumulated Wh (v1.0.3+)
 $SK 0^2C - set accumulated Wh to 0
SL 1|2|A  - set service level L1/L2/Auto
 $SL 1*14
 $SL 2*15
 $SL A*24
ST starthr startmin endhr endmin - set timer
 $ST 0 0 0 0^23 - cancel timer

G0 - get EV connect state
 response: $OK connectstate
 connectstate: 0=not connected, 1=connected, 2=unknown
 -> connectstate is unknown when EVSE pilot is -12VDC
 $G0^53

G3 - get charging time limit
 response: $OK cnt
 cnt*15 = minutes
        = 0 = no time limit
 $G3^50

G4 - get auth lock (needs AUTH_LOCK defined and AUTH_LOCK_REG undefined)
 response: $OK lockstate autolockestate
  lockstate = 0=unlocked, =1=locked
  autolockstate = 0=disabled, 1=enabled
 $G4^57
G9 - read PZEM
 response $OK
GA - get ammeter settings
 response: $OK currentscalefactor currentoffset
 $GA^22

GC - get current capacity info
 response: $OK minamps hmaxamps pilotamps cmaxamps
 all values decimal
 minamps - min allowed current capacity
 hmaxamps - max hardware allowed current capacity MAX_CURRENT_CAPACITY_Ln
 pilotamps - current capacity advertised by pilot
 cmaxamps - max configured allowed current capacity (saved to EEPROM)
 n.b. maxamps,emaxamps values are dependent on the active service level (L1/L2)
 $GC^20

GD - get Delay timer
 response: $OK starthr startmin endhr endmin
   all values decimal
   if timer disabled, starthr=startmin=endhr=endmin=0
 $GD^27

GE - get settings
 response: $OK amps flags
 amps(dec): current capacity
 flags(hex): ECF_xxx
 $GE^26

GF - get fault + boot counters 
 response: $OK gfitripcnt nogndtripcnt stuckrelaytripcnt overcurenttripcnt bootcnt ovtambienttripcnt ovtshutdowntripcnt ovtpanictripcnt gfiselftestfailcnt (all values hex)
 maximum trip count = 0xFF for any counter
 max boot cnt = 0xFFFF
 $GF^25

GG - get charging current and voltage
 response: $OK milliamps millivolts
 $GG^24

GH - get cHarge limit
 response: $OK kWh
 kWh = 0 = no charge limit
 $GH^2B

GI - get MCU ID - requires MCU_ID_LEN to be defined
 response: $OK mcuid
 mcuid: AVR serial number
        mcuid is 6 ASCII characters followed by 4 hex digits
        first hex digit = FF for 328P
  WARNING: mcuid is guaranteed to be unique only for the 328PB. Uniqueness is
	unknown in 328P. The first 6 characters are ASCII, and the rest are
	hexadecimal.

GO get Overtemperature thresholds
 response: $OK ambientthresh irthresh
 thresholds are in 10ths of a degree Celcius
 $GO^2C
GP - get temPerature (v1.0.3+)
 response: $OK cx16 cx10 -2560
 cx16 - temperature in 16ths of degree Celcius
   -> divide by 16 to get float temperature
 cx10 - temperature in 10ths of degree Celcius
 NOTE: if multiple sensors, returns only the highest priority sensor's data
 $GP^33

GS - get state
 response: $OK evsestate elapsed pilotstate vflags
 evsestate(hex): EVSE_STATE_xxx
 elapsed(dec): elapsed charge time in seconds of current or last charging session
 pilotstate(hex): EVSE_STATE_xxx
 vflags(hex): EVCF_xxx
 $GS^30

GT - get time (RTC)
 response: $OK yr mo day hr min sec       yr=2-digit year
 $GT^37

GU - get energy usage (v1.0.3+)
 response: $OK Wattseconds Whacc
 Wattseconds - Watt-seconds used this charging session, note you'll divide Wattseconds by 3600 to get Wh
 Whacc - total Wh accumulated over all charging sessions (not including the last one), note you'll divide Wh by 1000 to get kWh
 $GU^36

GV - get version
 response: $OK firmware_version protocol_version
 $GV^35

GZ - get timeZone string
 reponse: $OK tzstring
 NOTE: This is *only* implemented as a virtual RAPI command in the WiFi module
 *NOT* supported in this firmware

P commands - powershare
PS M mode - set power share mode
 mode: 0=disable 1=enable
 when mode = 1, pilot PWM is turned off when transitioning to STATE C
  charging won't start until PWM is turned on with PS P command

PG C - get charging current
 reponse: $OK manow maavg
 manow(dec): instantaneous milliamps
 maavg(dec): average milliamps

T commands for debugging only #define RAPI_T_COMMMANDS
T0 milliamps - set fake charging current
 response: $OK
 $T0 75000

ZT - run self test

Z0 FOR TESTING RELAY_AUTO_PWM_PIN ONLY
Z0 closems holdpwm
   closems(dec) = # ms to apply DC to relay pin
   holdpwm(dec) = pwm duty cycle for relay hold 0-255
Z1 FOR AMMETER CALIBRATION must #define CALIBRATION
Z1
 fetch averaged raw reading from ammeter
 response: $OK rawmean
Z2 OE2 voltmeter: must #define CALIBRATION
 fetch raw adc hi/lo


 *
 */

#ifdef RAPI

#define RAPIVER "G7.0.1"

// must hold longest response buffer.. currently $GF: 00 00 00 00 0192 1d 00 00
#define ESRAPI_BUFLEN 32
#define ESRAPI_SOC '$' // start of command
#define ESRAPI_RSOC '`' // start of reply echoed command
#define ESRAPI_EOC 0xd // CR end of command
#define ESRAPI_SOS ':' // start of sequence id
#define ESRAPI_MAX_ARGS 10
// for RAPI_SENDER
#define RAPIS_TIMEOUT_MS 500
#define RAPIS_BUFLEN 20

#define INVALID_SEQUENCE_ID 0

class EvseRapiProcessor {
  char curCmd[2];
  int8_t bufCnt; // # valid bytes in buffer
  int8_t tokenCnt;
  uint8_t curReceivedSeqId;
  char *tokens[ESRAPI_MAX_ARGS];
#ifdef GPPBUGKLUDGE
  char *buffer;
public:
  void setBuffer(char *buf) { buffer = buf; }
private:
#else
  char buffer[ESRAPI_BUFLEN]; // input/output buffer
#endif // GPPBUGKLUDGE
  void appendSequenceId(char *s,uint8_t seqId);
#ifdef RAPI_SENDER
  uint8_t curSentSeqId;
  uint8_t getSendSequenceId();
  int8_t isAsyncToken();
  int8_t isRespToken();
#endif // RAPI_SENDER

  virtual int available() = 0;
  virtual int read() = 0;
  virtual void writeStart() {}
  virtual void writeEnd() {}
  virtual int write(uint8_t u8) = 0;
  virtual int write(const char *str) = 0;

  void reset() {
    buffer[0] = 0;
    bufCnt = 0;
  }

  int tokenize(char *buf);
  int processCmd();

  void response(uint8_t ok);
  
#ifdef RAPI_SENDER
  char sendbuf[RAPIS_BUFLEN]; // input buffer
  void _sendCmd(const char *cmdstr);
#endif // RAPI_SENDER
  void _appendChk(char *buf);
  void _send(char *str);
  
public:
  EvseRapiProcessor();

  int doCmd();
  void sendEvseState(uint8_t evsestate,uint8_t pilotstate,uint8_t currentcapacity,uint16_t vflags,int32_t ma,int8_t madir);
  void sendBootNotification(uint8_t postcode);
  void sendWifiResetReq();
  void writeStr(const char *msg) { writeStart();write(msg);writeEnd(); }
#ifndef BTN_MENU
  void sendBtnPress(int8_t btnpress);
#endif // !BTN_MENU



  virtual void init();

#ifdef RAPI_SENDER
  int8_t sendCmd(const char *cmdstr);
  int8_t receiveResp(unsigned long msstart);
#endif // RAPI_SENDER
};

#ifdef RAPI_SERIAL
class EvseSerialRapiProcessor : public EvseRapiProcessor {
  int available() { return Serial.available(); }
  int read() { return Serial.read(); }
  int write(uint8_t u8) { return Serial.write(u8); }
  int write(const char *str) { return Serial.write(str); }

public:
  EvseSerialRapiProcessor();
  void init();
};

extern EvseSerialRapiProcessor g_ESRP;
#endif // RAPI_SERIAL


#ifdef RAPI_I2C
class EvseI2cRapiProcessor : public EvseRapiProcessor {
  int available() { return Wire.available(); }
  int read() { return Wire.read(); }
  void writeStart() { Wire.beginTransmission(RAPI_I2C_REMOTE_ADDR); }
  void writeEnd() { Wire.endTransmission(); }
  int write(uint8_t u8) { return Wire.write(u8); }
  int write(const char *str) { return Wire.write(str); }

public:
  EvseI2cRapiProcessor();
  void init();
};

extern EvseI2cRapiProcessor g_EIRP;
#endif // RAPI_I2C

void RapiInit();
void RapiDoCmd();
uint8_t RapiSendEvseState(uint8_t force=0);
void RapiSendWifiResetReq();
void RapiSendBootNotification(uint8_t postcode);
#ifndef BTN_MENU
void RapiSendBtnPress(int8_t btnpress);
#endif // !BTN_MENU

#endif // RAPI
