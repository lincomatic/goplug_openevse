// derived from https://github.com/parlotto/TMP75 by SCL
#include "open_evse.h"

#ifdef USE_TMP75
TMP75::TMP75() :
  _isPresent(false)
{
}

int TMP75::begin()
{
  Wire.beginTransmission(TMP75_ADDRESS);
  Wire.write(TMP75_CONF_REG);
  Wire.write(TMP75_DEFAULT_CONFIG);
  if (!Wire.endTransmission()) {
    _isPresent = true;
    delay(100);
    // Setup Pointer Register to TEMP register
    Wire.beginTransmission(TMP75_ADDRESS);
    Wire.write(TMP75_TEMP_REG);
    return Wire.endTransmission();
  }
  else return 1;
}

// unscaled 12-bit signed data 1/16th degree C
// return C*10
int16_t TMP75::read(int16_t *unscaled)
{
  if (_isPresent) {
    // Setup Pointer Register to TEMP register
    // don't need to set pointer register, because we do it
    // in begin().. TMPx75 remembers the last value until it's
    // overwritten
    /*Wire.beginTransmission(TMP75_ADDRESS);
    Wire.write(TMP75_TEMP_REG);
    Wire.endTransmission();
    */
    // Read temperature value
    Wire.requestFrom(TMP75_ADDRESS,2);
    int16_t t = (Wire.read() << 8)|Wire.read();
    // n.b. data bits start at MSB... unused bits are all 0, and start at LSB
    // TMP75/TMP175 defaults to 12-bit
    // LM75B is fixed @ 11 bits
    // the code below is compatible w/ both
    // w/ the code below, the LSB is always 0 in 11-bit mode
    t >>= 4; // unused bits are at bottom
    // n.b. using signed int, so don't need this if (t & 0x0800) t |= 0xF000; // sign-extend negative numbers
    // t contains C * 16
    if (unscaled) *unscaled = t;
    return (t * 10) / 16;  // return C*10
  }
  else {
    if (unscaled) *unscaled = (int16_t)0x8000;
    return (int16_t)TEMPERATURE_NOT_INSTALLED;
  }
}

#ifdef notusing
 void TMP75::setHighLimit( float highLimit )
 {
   int msb = highLimit ;
    int lsb = (highLimit - msb) * 256;
    Wire.beginTransmission(TMP75_ADDRESS);
    Wire.write(TMP75_THIGH_REG);
    Wire.write(msb);
    Wire.write(lsb & 0xF0);
    Wire.endTransmission();
   
 }
 
 void TMP75::setLowLimit( float lowLimit ){
    
  
    int msb = lowLimit ;
    int lsb = (lowLimit - msb) * 256;
    Wire.beginTransmission(TMP75_ADDRESS);
    Wire.write(TMP75_TLOW_REG);
    Wire.write(msb);
    Wire.write(lsb & 0xF0);
    Wire.endTransmission();
   
 }
 
#endif // notusing
#endif // USE_TMP75
