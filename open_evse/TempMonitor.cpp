#include "open_evse.h"

#ifdef TEMPERATURE_MONITORING
static inline void wiresend(uint8_t x) {
#if ARDUINO >= 100
  Wire.write((uint8_t)x);
#else
  Wire.send(x);
#endif
}

static inline uint8_t wirerecv(void) {
#if ARDUINO >= 100
  return Wire.read();
#else
  return Wire.receive();
#endif
}

TempMonitor g_TempMonitor;

#ifdef MCP9808_IS_ON_I2C
void TempMonitor::readMcp9808()
{
  m_MCP9808_tempx10 = m_mcp9808.readAmbient(&m_MCP9808_tempx16);  // for the MCP9808
}
#endif


#ifdef USE_TMP75
void TempMonitor::readTmp75()
{
  m_TMP75_tempx10 = m_tmp75.read(&m_TMP75_tempx16);
}
#endif


#ifdef USE_DS3231
void TempMonitor::readDs3231()
{
    if (Ds3231IsPresent()) {
      // This code chunk below reads the DS3231 RTC's internal temperature sensor            
      Wire.beginTransmission(DS1307_ADDRESS);
      wiresend(uint8_t(0x0e));
      wiresend( 0x20 );               // write bit 5 to initiate conversion of temperature
      Wire.endTransmission();            
      
      Wire.beginTransmission(DS1307_ADDRESS);
      wiresend(uint8_t(0x11));
      Wire.endTransmission();
      
      Wire.requestFrom(DS1307_ADDRESS, 2);
      m_DS3231_tempx10 = (((int16_t)wirerecv()) << 2) | (wirerecv() >> 6);
      if (m_DS3231_tempx10 == 0x3FF) {
        // assume chip not present
        m_DS3231_tempx10 = TEMPERATURE_NOT_INSTALLED;
        m_Flags |= TMF_DS3231_NOT_INSTALLED;
      }
      else {
        if (m_DS3231_tempx10 & 0x0200) m_DS3231_tempx10 |= 0xFE00; // sign extend negative number
        // convert from .25C to .1C
        m_DS3231_tempx16 = m_DS3231_tempx10 << 2; // C*16
        m_DS3231_tempx10 = (m_DS3231_tempx10 * 10) / 4;
      }
    }
}
#endif // USE_DS3231

void TempMonitor::Init()
{
  m_Flags = 0;

#ifdef USE_DS3231
  readDs3231();
#endif // USE_DS3231

#ifdef MCP9808_IS_ON_I2C
  m_mcp9808.begin();
  readMcp9808();
#endif // MCP9808_IS_ON_I2C

#ifdef USE_TMP75
  m_tmp75.begin();
  readTmp75();
#endif // USE_TMP75

}

void TempMonitor::Read()
{
  unsigned long curms = millis();
  if ((curms - m_LastUpdate) >= TEMPMONITOR_UPDATE_INTERVAL) {
#ifdef MCP9808_IS_ON_I2C
  readMcp9808();
#endif

#ifdef USE_TMP75
  readTmp75();
#endif

       
#ifdef USE_DS3231
  readDs3231();
#endif // USE_DS3231

    m_LastUpdate = curms;
  }
}

int8_t TempMonitor::TempExceeded(int16_t temperature)
{
  if (GetTempC10() >= temperature) return 1;
  else return 0;

  return 0;
}

// get highest priority temp
int16_t TempMonitor::GetTempC10()
{
#ifdef MCP9808_IS_ON_I2C
  if (Mcp9808IsPresent()) return m_MCP9808_tempx10;
#endif
#ifdef USE_TMP75
  if (Tmp75IsPresent()) return m_TMP75_tempx10;
#endif
#ifdef USE_DS3231
  if (Ds3231IsPresent()) return m_DS3231_tempx10;
#endif
  return TEMPERATURE_NOT_INSTALLED;
}


int16_t TempMonitor::GetTempC16()
{
#ifdef MCP9808_IS_ON_I2C
  if (Mcp9808IsPresent()) return m_MCP9808_tempx16;
#endif
#ifdef USE_TMP75
  if (Tmp75IsPresent()) return m_TMP75_tempx16;
#endif
#ifdef USE_DS3231
  if (Ds3231IsPresent()) return m_DS3231_tempx16;
#endif
  return TEMPERATURE_NOT_INSTALLED;
}



#endif // TEMPERATURE_MONITORING
