/*
 * Copyright (c) 2019-2022 Sam C. Lin
 */
// -*- C++ -*-
#pragma once

#ifdef KWH_RECORDING

#ifdef PZEM004T
#include "./SoftwareSerial.h"
#include "./PZEM004Tv30.h"
#endif // PZEM004T
#include "OEEnergyMeter.h"

// m_bFlags
#define EMF_IN_SESSION 0x01 // in a charging session
#define EMF_EV_CONNECTED 0x02
#define EMF_RELAY_CLOSED 0x04
class EnergyMeter {
  OEEnergyMeter _oeMeter;
#ifdef PZEM004T
  SoftwareSerial _pzemSerial;
  PZEM004Tv30 _pzem;
#endif // PZEM004T
  unsigned long m_lastUpdateMs;
  uint32_t m_wattHoursTot; // accumulated across all charging sessions
  uint32_t m_wattSeconds;  // current charging session
  uint8_t m_bFlags;

  uint8_t inSession() { return m_bFlags & EMF_IN_SESSION ? 1 : 0; }
  void setInSession() { setBits(m_bFlags,EMF_IN_SESSION); }
  void clrInSession() { clrBits(m_bFlags,EMF_IN_SESSION); }

  uint8_t evConnected() { return m_bFlags & EMF_EV_CONNECTED ? 1 : 0; }
  void setEvConnected() { setBits(m_bFlags,EMF_EV_CONNECTED); }
  void clrEvConnected() { clrBits(m_bFlags,EMF_EV_CONNECTED); }

  uint8_t relayClosed() { return m_bFlags & EMF_RELAY_CLOSED ? 1 : 0; }
  void setRelayClosed() { setBits(m_bFlags,EMF_RELAY_CLOSED); }
  void clrRelayClosed() { clrBits(m_bFlags,EMF_RELAY_CLOSED); }


  void _calcUsage(unsigned long dms);
  void startSession();
  void endSession();

public:
  EnergyMeter();

  void init();
  void Update();
  void SaveTotkWh();
  void SetTotkWh(uint32_t whtot) { m_wattHoursTot = whtot; }
  uint32_t GetTotkWh() { return m_wattHoursTot; }
  uint32_t GetSessionWs() { return m_wattSeconds; }

#ifdef PZEM004T
  uint32_t pzemMA() { return _pzem.mA(); }
  uint32_t pzemMV() { return _pzem.mV(); }
  uint32_t pzemMW() { return _pzem.mW(); }
  uint32_t pzemWh() { return _pzem.Wh(); }
  uint16_t pzemFreq() { return _pzem.freq(); }
  uint16_t pzemPF() { return _pzem.pf(); }
  int8_t readPZEM();
  bool pzemDetected() { return _pzem.deviceDetected(); }
#else
  bool pzemDetected() { return false; }
#endif // PZEM004T

  int8_t readAmmeter();
  OEEnergyMeter *getOEEnergyMeter() { return &_oeMeter; }
  uint32_t getVoltage();
  int32_t getInstantaneousMA();
  int32_t getAvgMA();
};


extern EnergyMeter g_EnergyMeter;
#endif // KWH_RECORDING


